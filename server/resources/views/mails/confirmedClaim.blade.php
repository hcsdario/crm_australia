@extends('layouts.email')

@section('content')
  <div class="title"><h1>La tua richiesta<br/>e‘ stata presa in carico </h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>Buongiorno {{user->name}} {{user->surname}},</b></p>
  <p><b>Ti confermiamo che l’intervento {{claim->category->name}} e‘ stato preso in carico.</b></p>
  <p><b>Affideremo al piu‘ presto l’intervento ad un artigiano.</b></p>

  <p><b>Clicca per visualizzare i dettagli della richiesta</b></p>
  <p><a class="btn" href="{{config('app.api_url')}}">Dettagli Intervento</span></p>
@endsection
