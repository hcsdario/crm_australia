@extends('layouts.email')

@section('content')
  <div class="title"><h1>Intervento COD. {{$claim->id}}<br/>Richiesta di Preventivo</h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>Buongiorno {{ $artisan->name }},</b></p>
  <p><b>Ha ricevuto una nuova richiesta di intervento in data <b>{{$claim->created_at}}</b> dal Cliente:</b></p>
  <h3>La contattiamo per richiedere il suo preventivo per l’intervento </h3>
  <div class="card">
    @php $user_img = $customer->images()->first(); @endphp
    <div class="avatar">@if($user_img) <img src="{{config('app.url').$user_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$customer->name}} {{$customer->surname}}</h4>
      <p>email. {{$customer->email}}</p>
      <p>codice cliente. {{$customer->id}}</p>
    </div>
  </div>
  <div class="card">
    @php $flat_img = $flat->image()->first(); @endphp
    <div class="avatar">@if($flat_img)<img src="{{config('app.url').$flat_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>Appartamento</h4>
      <p>{{$address}}</p>
      <p>codice appartmento. {{$flat->id}}</p>
    </div>
  </div>

  <p><b>Controlla sull'applicazione FlatMan tutti gli aggiornamenti</b></p>
@endsection
