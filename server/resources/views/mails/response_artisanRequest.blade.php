@extends('layouts.email')

@section('content')
  <div class="title"><h1>Intervento COD. {{$claim->id}}<br/>Accettato dall'Artigiano</h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>L’intervento elettrico {{$claim->id}} e‘ stato accettato dall’Artigiano:</b></p>
  <div class="card">
    @php $artisan_img = $artisan->images()->first(); @endphp
    <div class="avatar">@if($artisan_img) <img src="{{config('app.url').$artisan_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$artisan->name}}</h4>
      <p>email. {{$artisan->email}}</p>
      <p>codice ID. {{$artisan->id}}</p>
    </div>
  </div>
  <div>
    <h3>Data Accettata:</h3>
    @foreach ($proposed_dates as $key => $date)
      <h1 style="color:#54a2ab">{{date('d/m/Y h:m', strtotime($date->startAt))}} - {{date('h:m', strtotime($date->endAt))}}</h1>
    @endforeach
  </div>

  <h3 style="margin-top:2em;">Cliente:</h3>
  <div class="card">
    @php $user_img = $customer->images()->first(); @endphp
    <div class="avatar">@if($user_img) <img src="{{config('app.url').$user_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$customer->name}} {{$customer->surname}}</h4>
      <p>email. {{$customer->email}}</p>
      <p>codice cliente. {{$customer->id}}</p>
    </div>
  </div>

  <p><b>Clicca per visualizzare i dettagli della richiesta</b></p>
  <p><a class="btn" href="{{config('app.api_url')}}">Dettagli Intervento</span></p>
@endsection
