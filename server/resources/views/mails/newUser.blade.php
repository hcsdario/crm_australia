@extends('layouts.email')

@section('content')
  <div class="title"><h1>Benvenuto su FlatMan</h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>Benvenuto {{$user->name}} {{$user->surname}},</b></p>
  <p><b>sei stato invitato all' utilizzo dell'applicazione FlatMan.</b></p>
  <p><b>Accedi al tuo profilo con le seguenti credenziali:</b></p>
  <div class="card">
    @php $user_img = $user->images()->first(); @endphp
    <div class="avatar">@if($user_img) <img src="{{config('app.url').$user_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h2><small>Username:</small> <b><a>{{$user->email}}</a></b></h2>
      <h2><small>Password:</small> <b><a>{{$user->clear_password}}</a></b></h2>
    </div>
  </div>
  <hr/>
  @if ($user->type == 2)
  <p>D'ora in poi potrai gestire le richieste di intervento e i tuoi appuntamenti direttamente dall'app.</p>
  <hr/>
  @endif

  <p><b>Clicca per accedere con i tuoi dati all'applicazione</b></p>
  <p><a class="btn" href="{{config('app.api_url')}}">LINK ALLO STORE</span></p>
  <p>Ricordati di cambiare la password con una di tua preferenza al tuo primo accesso.</p>
@endsection
