@extends('layouts.email')

@section('content')
  <div class="title"><h1>Intervento COD. {{$claim->id}}<br/>Intervento concluso</h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>Il cliente ha segnalato l'intervento concluso:</b></p>
  <div class="card">
    @php $user_img = $customer->images()->first(); @endphp
    <div class="avatar">@if($user_img) <img src="{{config('app.url').$user_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$customer->name}} {{$customer->surname}}</h4>
      <p>email. {{$customer->email}}</p>
      <p>codice cliente. {{$customer->id}}</p>
    </div>
  </div>
  <div class="card">
    @php $flat_img = $flat->image()->first(); @endphp
    <div class="avatar">@if($flat_img)<img src="{{config('app.url').$flat_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>Appartamento</h4>
      <p>{{$address}}</p>
      <p>codice appartmento. {{$flat->id}}</p>
    </div>
  </div>
  <h3>Artigiani:</h3>
  <div class="card">
    @foreach ($claim->interventions as $intervention)
    @php $artisan = $intervention->artisan; @endphp
    @php $artisan_img = $intervention->artisan->images()->first(); @endphp
    <div class="avatar">@if($artisan_img)<img src="{{config('app.url').$artisan_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$artisan->name}}</h4>
      <p>email. {{$artisan->email}}</p>
      <p>codice cliente. {{$artisan->id}}</p>
    </div>
    @endforeach
  </div>

  <p><b>Clicca per visualizzare i dettagli della richiesta</b></p>
  <p><a class="btn" href="{{config('app.api_url')}}">Dettagli Intervento</span></p>
@endsection
