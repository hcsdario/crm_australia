@extends('layouts.email')

@section('content')
  <div class="title"><h1>La tua richiesta<br/>e‘ stata accettata </h1></div>
  <div class="separator">
    <div class="color-separator"></div>
  </div>
  <p><b>Buongiorno {{customer->name}} {{customer->surname}},</b></p>
  <p><b>Il tuo intervento e‘ stato accettato.</b></p>
  <br/>
  <div>
    @foreach ($proposed_dates as $key => $date)
      <h3>Prossimo appuntamento:</h3>
      <h1 style="color:#54a2ab">{{date('d/m/Y h:m', strtotime($date->startAt))}} - {{date('h:m', strtotime($date->endAt))}}</h1>
    @endforeach
  </div>
  <div class="card">
    @foreach ($claim->interventions as $intervention)
    @php $artisan = $intervention->artisan; @endphp
    @php $artisan_img = $intervention->artisan->images()->first(); @endphp
    <div class="avatar">@if($artisan_img)<img src="{{config('app.url').$artisan_img->thumb('100','100')}}" alt='No profile image'>@endif</div>
    <div class="card-body">
      <h4>{{$artisan->name}}</h4>
      <p>email. {{$artisan->email}}</p>
      <p>codice cliente. {{$artisan->id}}</p>
    </div>
    @endforeach
  </div>


  <p><b>Clicca per visualizzare i dettagli della richiesta</b></p>
  <p><a class="btn" href="{{config('app.api_url')}}">Dettagli Intervento</span></p>
@endsection
