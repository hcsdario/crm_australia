<style>
h1{
  font-size: 28px !important;
}
h2{
  font-size: 23px !important;
}
h3{
  font-size: 18px !important;
}
body *{
  font-family: 'Montserrat', sans-serif !important;
}
.header{
  position: relative;
  margin:0;
  padding:0px !important;
  width: 100%;
  height: 200px;
  display: block;
  background: #92DAE0;
  background: -moz-linear-gradient(273deg, #49948F 0, #92DAE0 100%);/* FF3.6+ */
  background: -webkit-gradient(linear, 273deg, color-stop(0, 49948F), color-stop(100%, 92DAE0));/* Chrome,Safari4+ */
  background: -webkit-linear-gradient(273deg, #49948F 0, #92DAE0 100%);/* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(273deg, #49948F 0, #92DAE0 100%);/* Opera 11.10+ */
  background: -ms-linear-gradient(273deg, #49948F 0, #92DAE0 100%);/* IE10+ */
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#1301FE', endColorstr='#F4F60C', GradientType='1'); /* for IE */
  background: linear-gradient(273deg, #49948F 0, #92DAE0 100%);/* W3C */
}
.header>.logo-container{
  position: relative;
  margin: auto;
  min-width: 500px;
  width: 70%;
  height: 100%;
  display: block;
}
.header .logo{
  position: relative;
  margin-top: 180px;
  display: block;
  width:33%;
  max-height: 120px;
  left: 0px;
  min-width:200px;
}
.title{
  margin-top: 3em;
}
.separator{
  position: relative;
  width: 100%;
  height: 4px;
  background-color: #dddddd;
  margin-bottom: 2em;
}
.btn{
  padding: 1em;
  color: white;
  font-weight: bold;
  background-color:#54a2ab;
  text-decoration: none;
  border-radius: 8px;
}
.color-separator{
  position: absolute;
  margin: 0;
  left: 0;
  top: 0;
  bottom: 0;
  width: 30%;
  background-color: #54a2ab;
}
.container{
  margin: auto;
  min-width: 500px;
  width: 70%;
  display: block;
}

.card{
  position: relative;
  display: flex;
  width: 100%;
  padding: 1em;
  margin-bottom: 1em;
}
.card>.avatar{
  position: relative;
  width: 100px;
  height: 100px;
  margin-top: 1em;
  padding: .5em;
  border-radius: 50%;
  background-color: #dddddd;
  float: left;
}
.avatar>img{
  position: relative;
  width: 100%;
  height: 100%;
  margin: 0px;
  border-radius: 50%;
}
.card>.card-body{
  padding-left: 1em;
  position: relative;
  width: auto;
  height: auto;
  float: left;
}
</style>
