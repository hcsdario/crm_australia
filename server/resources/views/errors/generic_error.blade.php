<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!-- CSRF Token -->
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <style>
    #background{
    background-image: url(http://hellocreativestudio.it/wp-content/uploads/2018/03/full01.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      height: 100vh;
      width: 100vw;
      margin: 0;
      position: fixed;
    }
    #app{
      font-family: 'Montserrat', sans-serif;
      width: 90vw;
      height: 50vh;
      margin: auto;
      left:0;
      right: 0;
      top:0;
      bottom: 0;
      position: absolute;
      display: block;
      text-align: center;
    }
    #error-container{
      text-align: initial;
      display: none;
    }
    #app>h1{
      color:#222;
      font-size:5rem;
    }
    #app>h2{
      color:#555;
      font-size:2rem;
    }
    #app>img{
      border-radius: 50%;
    }
    </style>
</head>
<body>
    <div id="background"></div>
    <div id="app">
      <img class="stnd  dark-version" alt="Hello Creative Studio" src="http://hellocreativestudio.it/wp-content/uploads/2018/03/HCS-128x1281.jpg" style="height: 80px;">
      <h1>OOPS... {{$exception instanceof  HttpException?$exception->getStatusCode(): ''}}</h1>
      <h2>Si è verificato un errore durante la gestione della tua richiesta.<br/>Riprovare più tardi oppure se il problema persiste contattare <a href="http://hellocreativestudio.it/" target="_blank">Hello Creative Studio</a></h2>
      <h3 onclick="window.history.back()" style="cursor:pointer;"><u>Clicca qui per tornate al sito</u></h3>
      <h6 onclick="showErrors()" style="cursor:pointer;text-align:right"><u>+ più informazioni...</u></h6>
      <div id="error-container">
        {!!$response!!}
      </pre>
      </div>

    </div>

    <!-- Scripts -->
    <script>
    function showErrors(){
      let el = document.querySelector("#error-container")
      if (el.style.display == 'none' || el.style.display=="" )
        el.style.display ='block'
      else
        el.style.display='none'
    }
    </script>
</body>
</html>
