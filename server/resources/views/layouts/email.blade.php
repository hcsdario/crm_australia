<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <!-- CSRF Token -->

    <title>{{ config('app.name') }} Avviso</title>

    <!-- Styles -->
    @include('mails.styles')
</head>
<body>
  <div class="header">
    <div class="logo-container">
      <img class="logo" src="{{config('app.url')}}/public/images/icon/LOGO_ESTESO_BIANCO.png" alt='FlatMan logo'/>
    </div>
  </div>
  <div class="container">
    @yield('content')
  </div>
  <div class="footer">
    <p>Grazie, il team di FlatMan</p>
    © {{ date('Y') }} {{ config('app.name') }}
  </div>
</body>
</html>
