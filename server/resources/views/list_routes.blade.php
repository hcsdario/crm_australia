<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                font-family: 'Raleway', sans-serif;
                font-weight: bold;
                height: 100vh;
                margin: 0;
            }

            #routes-table{
              position: absolute;
              margin: 2em;
              top:0;
              bottom: 0;
              right: 0;
              left: 0;
            }

            thead tr{
              color: grey;
            }
            tbody tr>td:nth-child(1){
              color: black;
            }
            tbody tr>td:nth-child(2){
              color: green;
            }

            tbody tr>td:nth-child(3){
              color: blue;
            }
            tbody tr:nth-child(2n+1){
              background-color: #ECEEF1;
            }
        </style>
    </head>
    <body>
      <table id="routes-table" class="table table-bordered table-responsive">
     <thead>
              <tr>
                  <th> uri </th>
                  <th> Name </th>
                  <th> Method </th>
              </tr>
     </thead>
     <tbody>
              @foreach ($routes as $route )
                  <tr>
                      <td>{{$route->uri}}</td>
                      <td>{{$route->getName()}}</td>
                      <td>{{ implode(', ',$route->methods())}}</td>
                  </tr>
              @endforeach
      </tbody>
</table>
    </body>
</html>
