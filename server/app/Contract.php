<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{


    protected $table = 'contracts';
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
	    "data",
	    "flat_id",
	    "user_id",
	    "operator_id"
    ];
    protected $casts = [
      'data' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function flat()
    {
        return $this->belongsTo('App\Flat');
    }
    public function files(){
      return $this->belongsToMany('App\File', 'contract_files', 'contract_id', 'file_id');
    }

}
