<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Intervention extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="interventions";
    protected $fillable = [
        'claim_id',
        'artisan_id',
        'completed',
        'urgency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [];

    public static function makeFromArtisanRequest($artisanRequest_id, $proposed_date_id = NULL){
      \DB::beginTransaction();
      $artisanRequest = ArtisanRequest::with(['evaluation'])->where(['id'=>$artisanRequest_id])->firstOrFail();
      $intervention = new Intervention;
      $intervention->claim_id = $artisanRequest->evaluation->claim_id;
      $intervention->artisan_id = $artisanRequest->artisan_id;
      $intervention->urgency = $artisanRequest->urgency;
      $intervention->completed = 0;

      $artisan_task = new ArtisanTask;
      $proposed_date;
      if ($proposed_date_id){
        // creo primo artisan task associato all'evento prelevando le date da una proposed_date
        $proposed_date = ProposedDate::findOrFail($proposed_date_id);
      }else{
        // se non fornisco un $proposed_date_id prendo la prima proposedDate dell' artisanRequest
        $proposed_date = $artisanRequest->proposedDates()->firstOrFail();
      }
      $artisan_task->startAt = $proposed_date->startAt;
      $artisan_task->endAt = $proposed_date->endAt;

      $intervention->save();
      $artisan_task->intervention_id = $intervention->id;
      $artisan_task->save();
      \DB::commit();

      return $intervention;
    }

    public function artisan(){
      return $this->hasOne('App\User','id','artisan_id');
    }
    public function claim(){
      return $this->hasOne('App\Claim','id','claim_id');
    }

    public function artisan_tasks(){
      return $this->hasMany('App\ArtisanTask');
    }

}
