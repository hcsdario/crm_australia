<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class infoCustomer extends Model
{
    protected $table = 'info_customers';
    protected $fillable = ['user_id','phone'];

    public function user(){
      return $this->belongsTo('App\User');
    }

}
