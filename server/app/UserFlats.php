<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFlats extends Model
{

    use SoftDeletes;


    protected $table ='user_flats';

    protected $fillable = [
        'user_id', 'flat_id'
    ];


    public function flat(){
        return $this->belongsToMany('App\Flat','user_flats','id','flat_id');
    }

    public function user(){
      return $this->belongsToMany('App\User');
    }



}
