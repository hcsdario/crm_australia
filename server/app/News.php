<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{


    protected $table = 'news';
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
	    "text",
	    "name",
	    "sended_at"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function users()
    {
        return $this->belongsToMany('App\User','news_users','news_id','user_id');
    }
    public function files(){
      return $this->belongsToMany('App\File', 'news_files', 'news_id', 'file_id');
    }

}
