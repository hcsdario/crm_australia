<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Flat extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'internal','floor','rooms_num', 'scale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function building()
    {
        return $this->belongsTo('App\Building');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_flats', 'flat_id', 'user_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    public function test()
    {
        return $this->belongsTo('App\User');
    }


}
