<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtisanRequest extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'artisans_requests';

    protected $fillable = [
        "evaluation_id",
        "artisan_id",
        "accepted",
        "urgency"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static $PENDING = 0;
    public static $ACCEPTED = 1;
    public static $REJECTED = 2;
    public static $CONFIRMED = 3;

    public function evaluation(){
      return $this->hasOne('App\Evaluation','id','evaluation_id');
    }

    public function proposedDates(){
      return $this->hasMany('App\ProposedDate','artisan_request_id');
    }

    public function artisan(){
      return $this->belongsTo('App\User','artisan_id');
    }
}
