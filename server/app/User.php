<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    // type codes
    public static $OPERATOR = 0;
    public static $CUSTOMER = 1;
    public static $ARTISAN = 2;
    public static $ID_AGENCY_ARTISAN = 1; // id del'agenzia come artigiano

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password','type', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
    protected $dates = ['deleted_at'];
    public function getInfo($value)
    {
        if ($this->info_customer) return $this->info_customer;
        else if ($this->info_artisan) return $this->info_artisan;
        else if ($this->info_operator) return $this->info_operator;
        else return [];
    }

    public function claims(){
      return $this->hasMany('App\Claim');
    }

    public function interventions(){
      return $this->hasMany('App\Intervention','artisan_id','id');
    }

    public function images(){
      return $this->belongsToMany('App\Image', 'users_images', 'user_id', 'image_id');
    }
    public function flats(){
      return $this->belongsToMany('App\Flat','user_flats', 'user_id', 'flat_id');
    }

    public function info(){
      if  ($this->type == User::$OPERATOR)
        return $this->hasOne('App\infoOperator');
      else if  ($this->type == User::$ARTISAN)
        return $this->hasOne('App\infoArtisan');
      else if  ($this->type == User::$CUSTOMER)
        return $this->hasOne('App\infoCustomer');
    }
    public function info_customer(){
      return $this->hasOne('App\infoCustomer');
    }
    public function info_artisan(){
      return $this->hasOne('App\infoArtisan');
    }
    public function artisan_request(){
      return $this->hasMany('App\ArtisanRequest', 'artisan_id');
    }
    public function buildings(){
      return $this->hasOne('App\Building');
    }

    public static function getAgency(){
      return User::findOrFail(User::$ID_AGENCY_ARTISAN);
    }


}
