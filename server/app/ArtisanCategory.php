<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtisanCategory extends Model
{


    protected $table = 'artisan_categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    "name"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function artisan(){
      return $this->belongsToMany('App\Artisan','artisans_categories' ,'artisan_category_id', 'user_id');
    }

}
