<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class infoArtisan extends Model
{
    protected $table = 'info_artisans';
    protected $fillable = ['user_id', 'telephone', 'address', 'piva', 'identificativo_aziendale'];

    public function user(){
      return $this->belongsTo('App\User');
    }
    public function categories(){
      return $this->belongsToMany('App\ArtisanCategory','artisans_categories' ,'user_id', 'artisan_category_id');
    }

}
