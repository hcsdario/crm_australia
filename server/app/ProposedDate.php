<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProposedDate extends Model
{


    protected $table = 'proposed_dates';
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "artisan_request_id",
        "startAt",
        "endAt"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function artisanRequest(){
      return $this->belongsTo('App\ArtisanRequest','artisan_request_id' ,'id');
    }

}
