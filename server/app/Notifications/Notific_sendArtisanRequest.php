<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Notific_sendArtisanRequest extends Notification
{
    use Queueable;
    protected $reference;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arg)
    {
        $this->reference = $arg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $artisan_request = $this->reference;
        $claim = \App\Claim::findOrFail($artisan_request->evaluation()->first()->claim_id);
        $customer = $claim->user()->first();
        $flat = $claim->flat()->first();
        $building = $flat->building()->first();
        $artisan = $artisan_request->artisan()->first();

        return (new MailMessage)
          ->subject('Nuova Richiesta di Intervento COD. '.$claim->id)
          ->view('mails.send_artisanRequest', ['customer'=>$customer, 'artisan' => $artisan, 'artisan_request'=>$artisan_request, 'claim'=>$claim, 'flat'=>$flat, 'address'=>$building->address.', '.$building->city.', '.$building->zipCode.' '.$building->country]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
      return [
          'type'=>'artisan_request',
          'message'=>'Richiesta di Intervento',
          'to_user'=>$notifiable->id,
          'id'=>$this->reference->evaluation()->first()->claim_id, //claim_id
          'data'=>$this->reference,
      ];
    }
}
