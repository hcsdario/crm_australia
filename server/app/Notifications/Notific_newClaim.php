<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Notific_newClaim extends Notification
{
    use Queueable;
    protected $reference;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arg)
    {
        $this->reference = $arg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->type == \App\User::$OPERATOR){
          $claim = $this->reference;
          $customer = $claim->user()->first();
          $flat = $claim->flat()->first();
          $building = $flat->building()->first();
          return (new MailMessage)
            ->subject('Nuova Richiesta di Intervento COD. '.$claim->id)
            ->view('mails.newClaim_agency', ['customer'=>$customer, 'claim'=>$claim, 'flat'=>$flat, 'address'=>$building->address.', '.$building->city.', '.$building->zipCode.' '.$building->country]);
          }
        if($notifiable->type == \App\User::$CUSTOMER){
          $claim = $this->reference;
          $customer = $claim->user()->first();
          return (new MailMessage)
            ->subject('La tua richiesta e‘ stata presa in carico')
            ->view('mails.newClaim_customer', ['customer'=>$customer, 'claim'=>$claim]);
          }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
      return [
          'type'=>'claim',
          'message'=>'Nuovo Intervento #'.$this->reference->id,
          'to_user'=>$notifiable->id,
          'id'=>$this->reference->id,
          'data'=>$this->reference,
      ];
    }
}
