<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Notific_responseArtisanRequest extends Notification
{
    use Queueable;
    protected $reference;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arg)
    {
        $this->reference = $arg;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $artisan_request = $this->reference;
      $claim = \App\Claim::findOrFail($artisan_request->evaluation()->first()->claim_id);
      $customer = $claim->user()->first();
      $proposed_dates = $artisan_request->proposedDates()->get();
      $artisan = $artisan_request->artisan()->first();
      return (new MailMessage)
        ->subject('Risposta dall\' Artigiano - Intervento COD. '.$claim->id)
        ->view('mails.response_artisanRequest', ['customer'=>$customer, 'artisan' => $artisan, 'claim'=>$claim, 'proposed_dates'=>$proposed_dates]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
      return [
          'type'=>'artisan_request',
          'message'=>'Risposta dall\' Artigiano #'.$this->reference->artisan()->first()->id,
          'to_user'=>$notifiable->id,
          'id'=>$this->reference->evaluation()->first()->claim_id, // claim_id
          'data'=>$this->reference,
      ];
    }
}
