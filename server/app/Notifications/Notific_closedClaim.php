<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Notific_closedClaim extends Notification
{
    use Queueable;
    protected $reference;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arg)
    {
        $this->reference = $arg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
         $claim = $this->reference;
         $claim->load('interventions.artisan');
         $customer = $claim->user()->first();
         $flat = $claim->flat()->first();
         $building = $flat->building()->first();
         return (new MailMessage)
           ->subject('Intervento COD. '.$claim->id.' concluso')
           ->view('mails.closedClaim', ['customer'=>$customer, 'claim'=>$claim, 'flat'=>$flat, 'address'=>$building->address.', '.$building->city.', '.$building->zipCode.' '.$building->country]);
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type'=>'claim',
            'message'=>'Intervento #'.$this->reference->id.' Completato',
            'to_user'=>$notifiable->id,
            'id'=>$this->reference->id,
            'data'=>$this->reference,
        ];
    }
}
