<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use \App\User;

class Notific_newUser extends Notification
{
    use Queueable;
    protected $reference;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($arg)
    {
        $this->reference = $arg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $notifiable)
    {
        $user = $this->reference;
        if ($user->type == User::$ARTISAN){
          return (new MailMessage)
                    ->subject('Benvenuto su FlatMan')
                    ->view('mails.newUser', ['user'=>$user]);
        }
        elseif ($user->type == User::$CUSTOMER){
          return (new MailMessage)
                    ->subject('Benvenuto su FlatMan')
                    ->view('mails.newUser', ['user'=>$user]);
        }else{
          return (new MailMessage)
                    ->subject('Benvenuto su FlatMan')
                    ->view('mails.newUser', ['user'=>$user]);
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
      return [
          'type'=>'customer',
          'message'=>'Nuovo Utente #'.$this->reference->id,
          'date'=> date('HH:mm dd/MM/YY'),
          'to_user'=>$notifiable->id,
          'id'=>$this->reference->id,
          'data'=>$this->reference,
      ];
    }
}
