<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluation extends Model
{


    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'evaluations';

    protected $fillable = [
        "operator_id",
        "customer_id",
        "claim_id",
        "note"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function operator(){
      return $this->hasOne('App\User','id','operator_id');
    }

    public function customer(){
      return $this->hasOne('App\User','id', 'customer_id');
    }
    public function claim(){
      return $this->hasOne('App\Claim','id','claim_id');
    }
    public function artisanRequest(){
      return $this->hasMany('App\ArtisanRequest');
    }

}
