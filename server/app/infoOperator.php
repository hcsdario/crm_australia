<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class infoOperator extends Model
{
    protected $table = 'info_operators';
    protected $fillable = ['user_id'];

    public function user(){
      return $this->belongsTo('App\User');
    }

}
