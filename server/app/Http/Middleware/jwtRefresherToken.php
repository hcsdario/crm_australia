<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class jwtRefresherToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        // file_put_contents ( 'logggg' , json_encode($response->getOriginalContent()) );
        $data = $response->getOriginalContent();
        $data['token'] = JWTAuth::refresh(JWTAuth::getToken());
        JWTAuth::setToken($data['token']);

        return  response()->json($data);
    }
}
