<?php

namespace App\Http\Middleware;

use Closure;
use DB;
class SuperLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ROUTE
        $path = $request->path();
        // eccezioni per bypassare middelware
        if (strpos($path, 'images/thumb') !== false || strpos($path, 'images/scale') !== false) return $next($request);
        // METHOD
        $method = $request->method();
        // REQUEST
        $req_fields = $request->all();
        // FILES
        // sostituisce file con nome_file@mime@dimensione
        foreach ($req_fields as $key => $filed) {
          if ($request->hasFile($key)){
            // per ogni file
            $req_fields[$key] = [];
            foreach ($request->file($key) as $fkey => $file) {
              $req_fields[$key] []= $file->getClientOriginalName().'@'.$file->getMimeType().'@'.$file->getSize();
            }
          }
        }
        $req_fields = json_encode($req_fields);
        // USER
        $user = auth()->user();
        $user_id = NULL;
        //se è un utente registrato loggo le sue informazioni
        if ($user) $user_id = $user->id;

        DB::insert('insert into logs (user_id, path, method, request, time) values (?, ?, ?, ?, ?)', [$user_id, $path, $method, $req_fields, date('Y-m-d H:i:s')]);

        return $next($request);
    }
}
