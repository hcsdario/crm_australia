<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\File;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $news = News::with(['users'])->orderBy('id','desc')->get();
      return response()->json(['news'=>$news]);
    }

    public function get($id)
    {
      $news = News::with(['user','files'])->find($id);
      return response()->json(['news'=>$news]);
    }

    public function send($id)
    {
      $news = News::firstOrFail(['id'=>$id]);
      return response()->json(['news'=>$news]);
    }


    public function store($id=NULL, Request $req)
    {
      $news = News::firstOrNew(['id'=>$id]);
      $news->data = $req->input('title');
      $news->flat_id = $req->input('text');
      $news->operator_id = auth()->user()->id;
      $news->save();

      if(!$id){
        $user = \App\User::findOrFail($news->user_id);
        $user->flats()->attach($news->flat_id);
      }
      if($req->input('users')){ // synch users
        $users_id =[];
        foreach($user as $req->input('users')){
          $users_id[]=$user['id'];
        }
        $news->users()->synch($users_id);
      }

      return response()->json(['news'=>$news]);
    }
    public function uploadFiles($id, Request $req)
    {
      $news = News::findOrFail($id);
      $files = $req->file('files');
      if (!$files || count($files)<1) return response('NO FILES', 400);  // BAD REQUEST

      foreach($files as $file){
        $up_file = File::upload($file);
        if (!$up_file)
           return response('INVALID FILE', 400);  // BAD REQUEST

        $up_file->save();
        $news->files()->attach($up_file->id);
      }
      return response()->json(['news'=>$news, 'files'=>$news->files()->get()]);
    }
    public function deleteFile($id, $file_id)
    {
      $news = News::findOrFail($id);
      $file = File::findOrFail($file_id);

      $news->files()->detach($file->id);
      $file->delete(); //TODO: rimuovere file dal server
      return response()->json(['news'=>$news]);
    }

    public function delete($id)
    {
      $news = News::findOrFail($id);
      $news->delete();
      return response()->json(['success'=>true]);
    }


}
