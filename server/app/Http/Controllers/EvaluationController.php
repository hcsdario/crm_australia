<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EvaluationRequest;  //  into ValidatedRequest
use App\Evaluation;

class EvaluationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $evaluation = Evaluation::get();
      return response()->json(['evaluation'=>$evaluation]);
    }

    public function get($id)
    {
      $evaluation = Evaluation::find($id);
      return response()->json(['evaluation'=>$evaluation]);
    }

    public function get_relations($id)
    {
      $evaluation = Evaluation::with(['operator','customer','claim','claim.images','artisanRequest','artisanRequest.artisan','artisanRequest.artisan.images'])->findOrFail($id);
      return response()->json(['evaluation'=>$evaluation]);
    }


    public function store($id=NULL, EvaluationRequest $req)
    {

      $evaluation = Evaluation::firstOrNew(['id'=>$id]);

      if($req->has('operator_id'))
        $evaluation->operator_id = $req->input('operator_id');
      if($req->has('customer_id'))
        $evaluation->customer_id = $req->input('customer_id');
      if($req->has('claim_id'))
        $evaluation->claim_id = $req->input('claim_id');
      if($req->has('note'))
        $evaluation->note = $req->input('note');

      if(!$id){
        $claim = \App\Claim::findOrFail($evaluation->claim_id);
        $claim->status_id=1;
        $claim->save();
      }

      $evaluation->save();

      return response()->json(['evaluation'=>$evaluation]);
    }

    public function delete($id)
    {
      $evaluation = Evaluation::findOrFail($id);
      $evaluation->delete();
      return response()->json(['success'=>true]);
    }


}
