<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArtisanCategory;

class ArtisanCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $artisanCategories = ArtisanCategory::get();
      return response()->json(['artisan_categories'=>$artisanCategories]);
    }

}
