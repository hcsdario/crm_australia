<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Category;
use App\Room;
use App\Problem;
use App\Available;
use App\Http\Requests\RoomRequest;  //  into ValidatedRequest

class RoomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $room = Room::get();
      return response()->json(['room'=>$room]);
    }

    public function get($id)
    {
      $room = Room::find($id);
      return response()->json(['room'=>$room]);
    }


    public function store($id=NULL, RoomRequest $req)
    {

      $room = Room::firstOrNew(['id'=>$id]);
      $room->name = $req->input('name');
      $room->save();

      return response()->json(['room'=>$room]);
    }

    public function delete($id)
    {
      $room = Room::findOrFail($id);
      $room->delete();
      return response()->json(['success'=>true]);
    }

    
}
