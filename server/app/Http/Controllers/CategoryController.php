<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Category;
use App\Room;
use App\Problem;
use App\Available;
use App\Http\Requests\CategoryRequest;  //  into ValidatedRequest

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $category = Category::get();
      return response()->json(['category'=>$category]);
    }

    public function get($id)
    {
      $category = Category::find($id);
      return response()->json(['category'=>$category]);
    }


    public function store($id=NULL, CategoryRequest $req)
    {

      $category = Category::firstOrNew(['id'=>$id]);
      $category->name = $req->input('name');
      $category->save();

      return response()->json(['category'=>$category]);
    }

    public function delete($id)
    {
      $category = Category::findOrFail($id);
      $category->delete();
      return response()->json(['success'=>true]);
    }

    
}
