<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spipu\Html2Pdf\Html2Pdf;

class pdfClass extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
//      $status = Status::get();
//      return response()->json(['status'=>$status]);
    }

    public function get($id)
    {
//      $status = Status::find($id);
//      return response()->json(['status'=>$status]);
    }

    public function getpdf ($path){
      if($path && strpos($path, '..') == false){
        return response()->file(base_path().'/upload/pdf/'.$path);
      }
    }

    public function topdf($id)
    {

/*		require_once(base_path().'/vendor/spipu/html2pdf/src/Html2Pdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Locale.php');
		require_once(base_path().'/vendor/tecnickcom/tcpdf/tcpdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/MyPdf.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/CssConverter.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Debug/DebugInterface.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Debug/Debug.php');
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Parsing/*.php') as $filename)
			require_once($filename);
		require_once(base_path().'/vendor/spipu/html2pdf/src/Extension/ExtensionInterface.php');

		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Extension/*.php') as $filename)
			require_once($filename);
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Exception/*.php') as $filename)
			require_once($filename);
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/TagInterface.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/AbstractTag.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/AbstractDefaultTag.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/I.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/S.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/Span.php');
		require_once(base_path().'/vendor/spipu/html2pdf/src/Tag/U.php');
		foreach (glob(base_path().'/vendor/spipu/html2pdf/src/Tag/*.php') as $filename)
			require_once($filename);*/

        //        $debug = new \Spipu\Html2Pdf\Debug\Debug();

        $user_id = Auth::user()->id;
        $content = $_POST['html'];
        try {
            ob_start();
            $html2pdf = new Html2Pdf('P', 'A4', 'it', true);
        //            $html2pdf->setModeDebug($debug);
		    $html2pdf->writeHTML($content);
            $pdf_name =$_POST['name'].'_'.time().'.pdf';
			$html2pdf->output( base_path().'/upload/pdf/'.$pdf_name, 'F');
    		} catch (Html2PdfException $e) {
    			echo $html2pdf->clean();
    			$formatter = new ExceptionFormatter($e);
    			echo $formatter->getHtmlMessage();
    	}

        return json_encode(['pdf_name'=>$pdf_name]);

    }


    public function store($id=NULL, CategoryRequest $req)
    {

/*      $status = Status::firstOrNew(['id'=>$id]);
      $status->name = $req->input('name');
      $status->save();
      return response()->json(['status'=>$status]);*/
    }

    public function delete($id)
    {
/*      $status = Status::findOrFail($id);
      $status->delete();
      return response()->json(['success'=>true]);*/
    }

    
}
