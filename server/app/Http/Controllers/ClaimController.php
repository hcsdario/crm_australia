<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Category;
use App\Room;
use App\Problem;
use App\Status;
use App\Available;
use App\Http\Requests\ClaimRequest;  //  into ValidatedRequest
use App\Http\Requests\ImageAddRequest;  //  into ValidatedRequest
use Illuminate\Support\Facades\Auth;
use App\Image;

use Illuminate\Support\Facades\DB;

class ClaimController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function all()
    {
      $claims = Claim::with(['flat', 'user', 'category'])->get();
      return response()->json(['claims'=>$claims]);
    }

    public function claimsOpen()
    {
      $claims = Claim::with(['flat', 'user', 'category','status'])->where("status_id",'>=',1)->get();
      return response()->json(['claims'=>$claims]);
    }

    public function all_relations()
    {
      $claims = Claim::with(['category', 'room','problem','status'])->orderBy('id','desc')->get();
      return response()->json(['claims'=>$claims]);
    }

    public function user_claims($user_id)
    {
      $user_claims=Claim::with(['category', 'room','problem','flat.building','status'])->orderBy("created_at","desc")->get();
      return response()->json(['claims'=>$user_claims]);
    }

    public function get($id)
    {
      $claim = Claim::with(['flat', 'user', 'images','flat.building'])->find($id);
      return response()->json(['claim'=>$claim]);
    }

    public function get_relations($id)
    {
      $claim = Claim::with(['category', 'room','problem','flat.building','status','availables','images','user', 'flat', 'evaluation','interventions','interventions.artisan_tasks','interventions.artisan','interventions.artisan.images','interventions.artisan.info_artisan'])->orderBy("created_at","desc")->find($id);
      return response()->json(['claim'=>$claim]);
    }



    protected function reverse_date($data){
      $parts=explode(" ", $data);
      if(count($parts)>1)
      {

        $data_parts=explode("/", $parts[0]);
        if(count($data_parts)>1)
          return $data_parts[2]."/".$data_parts[1]."/".$data_parts[0]." ".$parts[1];
        else{
        $data_parts=explode("-", $parts[0]);
        if(count($data_parts)>1)
          return $data_parts[2]."/".$data_parts[1]."/".$data_parts[0]." ".$parts[1];
        }
      }
    }

    public function store($id=NULL, ClaimRequest $req)
    {
      $claim = Claim::firstOrNew(['id'=>$id]);
      $claim->user_id = $req->input('user_id');
      $claim->flat_id = $req->input('flat_id');
      if ($req->has('note'))
        $claim->note = $req->input('note');
      $claim->category_id = $req->input('category_id');
      $claim->room_id = $req->input('room_id');
      $claim->problem_id = $req->input('problem_id');
      $claim->save();

      $dates=$req->input('dates');
      foreach ($dates as $start_end) {
        $avaible =  Available::create(
        [
            'claim_id' => $claim->id,
            'startAt' =>  $start_end['startAt'],
            'endAt' => $start_end['endAt']
        ]);
      }

     //  appena creato
     if(!$id){
       auth()->user()->notify(new \App\Notifications\Notific_newClaim($claim));
       \App\User::getAgency()->notify(new \App\Notifications\Notific_newClaim($claim));
     }

      return response()->json(['claim'=>$claim]);
    }

    public function add_claims_availabilities($claim_id, $startAt,$endtAt)
    {
      $data=[
        "claim_id" => $claim_id,
        "startAt" => $startAt,
        "endtAt" => $endtAt
      ];
      DB::table("availabilities")->insert($data);
      return response()->json(['message'=>"success"]);
    }


    public function completed($id)
    {
      $claim = Claim::firstOrNew(['id'=>$id]);
      $claim->completed = 1;

      $claim->save();
      auth()->user()->notify(new \App\Notifications\Notific_completedClaim($claim));

      return response()->json(['claim'=>$claim]);
    }

    public function app_form_boot(){


      $problems=DB::select("SELECT claims_problems.id as value,claims_problems.name as label, claims_problems.room_id as ind FROM `claims_problems`
                        INNER JOIN claims_categories ON claims_problems.category_id=claims_categories.id
                        INNER JOIN claims_rooms ON claims_problems.room_id=claims_rooms.id");
      $rooms=DB::select("SELECT id as value, name as label FROM `claims_rooms`");

      $response = [
      'claimId' =>0,
      'category_id'=>3,
      'room' => [
        'options' => $rooms,
        'value' => 0
      ],
      'problems' => [
        'options' => $problems,
        'value' => 0
      ],
      'kind' => 'tecnico generico',
      'images' => [],
      'imagesfileObject' => [],
      'startAt' => [],
      'endAt' => [],
      ];

      die(json_encode($response));
    }

    public function delete($id)
    {
      $claim = Claim::findOrFail($id);
      $claim->delete();
      return response()->json(['success'=>true]);
    }

    public function addImage($id, ImageAddRequest $req)
    {
      $claim = Claim::find($id);
      $image = NULL;
      if ($req->hasFile('image')){
        $image = Image::upload($req->file('image'));
        $claim->images()->attach($image->id);
        $claim->save();
      }
      return response()->json(['claim'=>$claim, 'images'=>$claim->images(),'message','uploaded']);
    }

    public function removeImage($id, $image_id)
    {
      $claim = Claim::find($id);
      $claim->images()->detach($image_id);
      $claim->save();
      return response()->json(['claim'=>$claim, 'image'=>$claim->images()]);
    }

    public function categories(){
      $sala = [["id"=>1,"name"=>"soggiorno", "options"=>[1,2,4,6]], ["id"=>1,"name"=>"bagno", "options"=>[3,4,5]]];
      return response()->json([]);
    }

    public function search(Request $req)
    {
        $params =  $req->all();
        $query = Claim::with(['category', 'room','problem','flat.building','status','availables','images','user', 'flat', 'evaluation','interventions','interventions.artisan_tasks','interventions.artisan','interventions.artisan.info_artisan'])->orderBy("created_at","desc");

        foreach ($params as $param => $value) {
          if (!$value) continue;
          switch ($param) {
            default:
              $query = $query->where($param, $value);
              break;
          }
        }
        $users = $query->limit(50)->get();
        return response()->json(['claims'=>$users, 'query'=>$params]);
    }

}
