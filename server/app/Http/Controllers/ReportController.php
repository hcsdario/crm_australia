<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\Claim;
use App\Intervention;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Load the application dashboard sources
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardSources()
    {
        $sources=[];
        $sources['graphsClaim']=$this->graphsClaim();
        $sources['graphsPhases']=$this->graphsPhases();
        return response()->json(['sources'=>$sources]);
    }

    /* ritorna le date inizio fine del mese corrente o le date del mese specificato in formato numerico come argomento (0 = gennaio ... 11=dicembre) */
    protected function getDatesFromMonth($monthIndex){

        $months=[
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        if($monthIndex===false)
            $time=date("F Y");
        else
            $time=date($months[$monthIndex]." Y");

        $timestamp    = strtotime($time);
        $first_second = date('Y-m-01', $timestamp);
        $last_second  = date('Y-m-t', $timestamp);

        $startAt="";
        $endAt="";
        return [
            'start' => $first_second,
            'end' => $last_second
        ];
    }


    protected function getInterventions($month=false){
        $time=$this->getDatesFromMonth($month);
        return Intervention::with('claim','claim.problem','claim.status','claim.user','claim.flat.building')->whereBetween('created_at', [$time['start'], $time['end']])->get();
    }

    /* sezione 4 */
    protected function graphsClaim($month=false){
        $time=$this->getDatesFromMonth($month);
        $claims = Claim::with('category')->where("status_id",Claim::$COMPLETATO)->whereBetween('created_at', [$time['start'], $time['end']])->get();
        $response = [];
        $total=0;
        foreach ($claims as $claim) {
            if(isset($response[$claim->category->name])){

                $response[$claim->category->name]['count']+=1;
            }
            else
                $response[$claim->category->name]=
                [
                'count' => 1,
                'percentage' => 0
                ];
            $total++;
        }
        $keys=array_keys($response);
        for($i=0;$i<count($keys);$i++)
            $response[$keys[$i]]['percentage'] =number_format( $response[$keys[$i]]['count'] * 100 / $total ,2);

        return $response;
    }


    /* sezione 5 */
    protected function graphsPhases($month=false){
        $time=$this->getDatesFromMonth($month);
        $claims = Claim::whereIn('status_id',[0,1,2,4])->whereBetween('created_at', [$time['start'], $time['end']])->get();
        $response=$claims;
        $response = [];
        $total=0;
        foreach ($claims as $claim) {
        	if(isset($response[$claim->status_id])){
        		$response[$claim->status_id]['count']+=1;
        	}
        	else
        		$response[$claim->status_id]=
	        	[
	        	'count' => 1,
	        	'percentage' => 0
	        	];
        	$total++;
        }
        $keys=array_keys($response);
        for($i=0;$i<count($keys);$i++)
        	$response[$keys[$i]]['percentage'] =number_format( $response[$keys[$i]]['count'] * 100 / $total ,2);

        return $response;
    }


    /* sezione 2 */
    protected function getLastClaims($limit=4)
    {
        /* affiche tutto funzioni gli stati di claims devono essere aggiornati bene */
        return
        DB::select("
        SELECT * FROM(
            SELECT
               'claims' as kind,
                0 as accepted,
                claims.status_id as priority,
                claims.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `claims`
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(claims.deleted_at)

            UNION ALL

            SELECT
               'evaluations' as kind,
                0 as accepted,
                claims.status_id as priority,
                evaluations.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `evaluations`
                INNER JOIN claims ON evaluations.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(evaluations.deleted_at) AND  isnull(claims.deleted_at)

            UNION ALL

            SELECT
               'artisans_requests' as kind,
                accepted,
                claims.status_id as priority,
                artisans_requests.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `artisans_requests`
                INNER JOIN evaluations ON artisans_requests.evaluation_id=evaluations.id
                INNER JOIN claims ON evaluations.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(artisans_requests.deleted_at) AND  isnull(evaluations.deleted_at)  AND isnull(claims.deleted_at)


            UNION ALL

            SELECT
               'interventions' as kind,
                0 as accepted,
                claims.status_id as priority,
                interventions.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `interventions`
                INNER JOIN claims ON interventions.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(interventions.deleted_at) AND isnull(claims.deleted_at)



        ) as interventi

        ORDER BY created_at DESC,priority ASC
        LIMIT $limit
        ");

    }


    protected function getGraphCategory($month=false){
        return Todo::whereDate("date", '<=', date('Y-m-d'))->get();;
    }

}
