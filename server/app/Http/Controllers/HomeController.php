<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\Claim;
use App\Intervention;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Load the application dashboard sources
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardSources()
    {
        $sources=[];
        $sources['todo']=$this->getTodo();
        $sources['lastClaims']=$this->getLastClaims();
        $sources['interventions']=$this->getInterventions();
        $sources['graphsClaim']=$this->graphsClaim();
        $sources['graphsPhases']=$this->graphsPhases();
        return response()->json(['sources'=>$sources]);
    }

    /* ritorna le date inizio fine del mese corrente o le date del mese specificato in formato numerico come argomento (0 = gennaio ... 11=dicembre) */
    protected function getDatesFromMonth($monthIndex){

        $months=[
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        if($monthIndex===false)
            $time=date("F Y");
        else
            $time=date($months[$monthIndex]." Y");

        $timestamp    = strtotime($time);
        $first_second = date('Y-m-01', $timestamp);
        $last_second  = date('Y-m-t', $timestamp);

        $startAt="";
        $endAt="";
        return [
            'start' => $first_second,
            'end' => $last_second
        ];
    }

    /* sezione 3 */
    // 4
    protected function calendarData($month=false){

	    $sql="
		SELECT
		CONCAT('richiesta:#',interventions.claim_id,' alle ore ',time(artisans_tasks.startAt)) as label_claim,
		interventions.claim_id as claim_id,
		CONCAT('richiesta da: ', users2.name,' ',users2.surname) as label_artisan,
		users2.id as users_id,
		CONCAT('richiesta:#',interventions.claim_id,' alle ore ',time(artisans_tasks.startAt),' presa in carico da: ', users2.name,' ',users2.surname ) as title,
		'artisan_tasks' as kind,
		artisans_tasks.startAt as startAt,
		artisans_tasks.endAt as endAt,
		artisans_tasks.note as note,
		date(artisans_tasks.startAt) as date
		FROM `interventions`
		INNER JOIN claims ON interventions.claim_id=claims.id
		INNER JOIN users users2 ON claims.user_id=users2.id
		INNER JOIN user_flats ON users2.id=user_flats.user_id
		INNER JOIN flats ON flats.id=user_flats.flat_id
		INNER JOIN artisans_tasks ON artisans_tasks.intervention_id = interventions.id
        GROUP BY interventions.claim_id,users2.name,users2.surname,artisans_tasks.startAt,users2.id,artisans_tasks.endAt,artisans_tasks.note
		";

		$int=DB::select($sql);

        $time=$this->getDatesFromMonth($month);

        $interventions = Intervention::with(['artisan_tasks' => function($query){
		    $query->orderBy('created_at', 'ASC')->get();
		},'claim','claim.problem'])->whereBetween('created_at', [$time['start'], $time['end']])->orderBy("created_at","ASC")->get();

        $todos = Todo::whereBetween('date', [$time['start'], $time['end']])->orderBy("date","ASC")->get();

        $response=[];
        foreach ($int as $artisan_tasks) {

	        	$response[]=[
                    'label_claim' => $artisan_tasks->label_claim,
                    'claim_id' => $artisan_tasks->claim_id,
                    'label_artisan' => $artisan_tasks->label_artisan,
                    'users_id' => $artisan_tasks->users_id,
                    'date' => $artisan_tasks->date,
	        		'kind' => 'artisan_tasks',
	        		'startAt' => $artisan_tasks->startAt,
	        		'endAt' => $artisan_tasks->endAt,
	        		'note' => $artisan_tasks->note,
	        		'title' => $artisan_tasks->title
	        	];
        }


        foreach ($todos as $todo) {

        	$date=$todo->date;

        	$response[]=[
                'date' => $date,
                'kind' => 'todo',
        		'todo_id' => $todo->id,
                'startAt' => $todo->startAt,
                'endAt' => $todo->endAt,
        		'k' => 'k',
        		'title' => $todo->note
        	];
        }
        $response['int']=$int;
        return response()->json(['results' => $response]);
    }

    /* sezione 3 */
    // 4
    protected function getInterventions($month=false){
        $time=$this->getDatesFromMonth($month);
        return Intervention::with('claim','claim.problem','claim.status','claim.user','claim.flat.building')->whereBetween('created_at', [$time['start'], $time['end']])->get();
    }

    /* sezione 4 */
    protected function graphsClaim($month=false){
        $time=$this->getDatesFromMonth($month);
        $claims = Claim::with('category')->where("status_id",Claim::$COMPLETATO)->whereBetween('created_at', [$time['start'], $time['end']])->get();
        $response = [];
        $total=0;
        foreach ($claims as $claim) {
            if(isset($response[$claim->category->name])){

                $response[$claim->category->name]['count']+=1;
            }
            else
                $response[$claim->category->name]=
                [
                'count' => 1,
                'percentage' => 0
                ];
            $total++;
        }
        $keys=array_keys($response);
        for($i=0;$i<count($keys);$i++)
            $response[$keys[$i]]['percentage'] =number_format( $response[$keys[$i]]['count'] * 100 / $total ,2);

        return $response;
    }


    /*
      0 In attesa visualizzazione
      1 Visualizzato
      2 Confermato
      3 Rischedulato
      4 Completato
      5 Annullato
    */



    /* sezione 5 */
    protected function graphsPhases($month=false){
        $time=$this->getDatesFromMonth($month);
        $claims = Claim::whereIn('status_id',[0,1,2,4])->whereBetween('created_at', [$time['start'], $time['end']])->get();
        $response=$claims;
        $response = [];
        $total=0;
        foreach ($claims as $claim) {
        	if(isset($response[$claim->status_id])){
        		$response[$claim->status_id]['count']+=1;
        	}
        	else
        		$response[$claim->status_id]=
	        	[
	        	'count' => 1,
	        	'percentage' => 0
	        	];
        	$total++;
        }
        $keys=array_keys($response);
        for($i=0;$i<count($keys);$i++)
        	$response[$keys[$i]]['percentage'] =number_format( $response[$keys[$i]]['count'] * 100 / $total ,2);

        return $response;
    }


    /* sezione 3 */
    protected function getTodo($month=false){
        return [
            'list' => Todo::whereDate("date", '<=', date('Y-m-d'))->where("completed",0)->get(),
            'active' => Todo::whereDate("date", '<=', date('Y-m-d'))->where("completed",0)->count(),
            'done' => Todo::whereDate("date", '<=', date('Y-m-d'))->where("completed",1)->count()
        ];
    }

    /* sezione 2 */
    protected function getLastClaims($limit=4)
    {
        /* affiche tutto funzioni gli stati di claims devono essere aggiornati bene */
        return
        DB::select("
        SELECT * FROM(
            SELECT
               'claims' as kind,
                0 as accepted,
                claims.status_id as priority,
                claims.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `claims`
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(claims.deleted_at)

            UNION ALL

            SELECT
               'evaluations' as kind,
                0 as accepted,
                claims.status_id as priority,
                evaluations.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `evaluations`
                INNER JOIN claims ON evaluations.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(evaluations.deleted_at) AND  isnull(claims.deleted_at)

            UNION ALL

            SELECT
               'artisans_requests' as kind,
                accepted,
                claims.status_id as priority,
                artisans_requests.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `artisans_requests`
                INNER JOIN evaluations ON artisans_requests.evaluation_id=evaluations.id
                INNER JOIN claims ON evaluations.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(artisans_requests.deleted_at) AND  isnull(evaluations.deleted_at)  AND isnull(claims.deleted_at)


            UNION ALL

            SELECT
               'interventions' as kind,
                0 as accepted,
                claims.status_id as priority,
                interventions.created_at,
                buildings.address,
                concat(users.name,' ',users.surname) as user,
                claims_status.name as status
            FROM `interventions`
                INNER JOIN claims ON interventions.claim_id=claims.id
                INNER JOIN flats ON claims.flat_id=flats.id
                INNER JOIN buildings ON flats.building_id=buildings.id
                INNER JOIN claims_status ON claims.status_id=claims_status.id
                INNER JOIN users ON claims.user_id=users.id
            WHERE isnull(interventions.deleted_at) AND isnull(claims.deleted_at)



        ) as interventi

        ORDER BY created_at DESC,priority ASC
        LIMIT $limit
        ");

    }


    protected function getGraphCategory($month=false){
        return Todo::whereDate("date", '<=', date('Y-m-d'))->get();;
    }

}
