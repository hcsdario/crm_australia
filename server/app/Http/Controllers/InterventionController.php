<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InterventionRequest;  //  into ValidatedRequest
use App\Intervention;
use App\Claim;

class InterventionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $intervention = Intervention::get();
      return response()->json(['intervention'=>$intervention]);
    }

    public function get($id)
    {
      $intervention = Intervention::find($id);
      return response()->json(['intervention'=>$intervention]);
    }
    public function get_relations($id)
    {
      $intervention = Intervention::with(['artisan_tasks','artisan', 'claim', 'claim.images', 'claim.category','claim.room','claim.problem','claim.user','claim.user.images','claim.user.info_customer','claim.evaluation','claim.flat', 'claim.flat.building'])->findOrFail($id);
      return response()->json(['intervention'=>$intervention]);
    }


    public function store($id=NULL, InterventionRequest $req)
    {
      $intervention = Intervention::firstOrNew(['id'=>$id]);
      $claim = Claim::findOrFail($intervention->claim_id);

      if($req->has('claim_id'))
        $intervention->claim_id = $req->input('claim_id');
      if($req->has('artisan_id'))
        $intervention->artisan_id = $req->input('artisan_id');
      if($req->has('completed'))
        $intervention->completed = $req->input('completed');
      if($req->has('urgency'))
        $intervention->urgency = $req->input('urgency');
      $intervention->save();

      $claim->status_id = Claim::$VISUALIZZATO;
      $claim->save();

      return response()->json(['intervention'=>$intervention]);
    }
    public function completed($id=NULL)
    {
      $intervention = Intervention::firstOrNew(['id'=>$id]);
      $claim = Claim::findOrFail($intervention->claim_id);
      $intervention->completed = 1;
      $intervention->save();

      if ($claim->interventions()->where('completed',0)->count() < 1){
        $claim->status_id = Claim::$COMPLETATO;
        $claim->save();
        \App\User::getAgency()->notify(new \App\Notifications\Notific_closedClaim($claim));
        \App\User::findOrFail($claim->user_id)->notify(new \App\Notifications\Notific_closedClaim($claim));
      }

      //TODO add notify
      // avviso artigiano Intervento chiuso
      // \App\User::findOrFail($intervention->artisan_id)->notify(new \App\Notifications\Notific_closedIntervention($intervention));
      $intervention->completed = 1;
      $intervention->save();

      return response()->json(['intervention'=>$intervention, 'count_interventions'=>$claim->interventions()->count(), 'count_uncompleted_interventions'=>$claim->interventions()->where('completed',0)->count()]);
    }

    public function artisan($user_id){
      $interventions = Intervention::with('artisan_tasks', 'claim', 'claim.category', 'claim.flat', 'claim.flat.building')->where(['artisan_id'=>$user_id])->orderBy('id','desc')->limit(200)->get();
      return response()->json(['interventions'=>$interventions]);
    }
    public function artisanFilterByStatus($user_id, $status){
      $interventions = Intervention::with('artisan_tasks', 'claim', 'claim.category', 'claim.user', 'claim.flat', 'claim.flat.building')
      ->where('artisan_id',$user_id)->where('completed', $status)->orderBy('id','desc')->limit(200)->get();
      return response()->json(['interventions'=>$interventions]);
    }

    public function delete($id)
    {
      $intervention = Intervention::findOrFail($id);
      $intervention->delete();
      return response()->json(['success'=>true]);
    }


}
