<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Building;
use App\Flat;
use App\Claim;
use \DB;

class SearchController extends Controller
{
  private $limit = 50;
  public function queryUser($value){
    $query = User::with(['images','flats'])->orderBy('id','desc')->where('type','<>',User::$OPERATOR)
      ->where(DB::raw('concat(name," ",surname)'), 'like', '%'.$value.'%')
      ->orWhere('id', $value)->orWhere('created_at', 'like', '%'.$value.'%')->limit($this->limit);
    return $query;
  }
  public function queryBuilding($value){
    $query = Building::with(['flats'])->orderBy('id','desc')
      ->where(DB::raw('concat(address," ",city," ",country," ",zipCode)'), 'like', '%'.$value.'%')
      ->orWhere('code', $value)->orWhere('created_at', 'like', '%'.$value.'%')->limit($this->limit);
    return $query;
  }
  public function queryFlat($value){
    $query = Flat::with(['users'])->orderBy('id','desc')
      ->whereHas('building', function($q) use($value) { $q->where(DB::raw('concat(address," ",city," ",country," ",zipCode)'), 'like', '%'.$value.'%'); })
      ->orWhere('code', $value)->orWhere('created_at', 'like', '%'.$value.'%')->limit($this->limit);
    return $query;
  }
  public function queryClaim($value){
    $query = Claim::orderBy('id','desc')
      ->orWhere('id', $value)->orWhere('created_at', 'like', '%'.$value.'%')->limit($this->limit);
    return $query;
  }

  public function globalSearch (Request $req){
    if (!$req->input('value')) return response()->json(['results'=>[]]);
    $users = $this->queryUser($req->input('value'))->get();
    $users->each(function ($item,$key){
      if ($item->type==User::$ARTISAN){ $item['kind'] = 'artisan';}
      else {$item['kind'] = 'customer';}
    });
    $buildings = $this->queryBuilding($req->input('value'))->get();
    $buildings->each(function ($item,$key){
      $item['kind'] = 'building';
    });
    $flats = $this->queryFlat($req->input('value'))->get();
    $flats->each(function ($item,$key){
      $item['kind'] = 'flat';
    });
    $claims = $this->queryClaim($req->input('value'))->get();
    $claims->each(function ($item,$key){
      $item['kind'] = 'claim';
    });

    $results = collect();
    $results = $results->concat($users)->concat($buildings)->concat($flats)->concat($claims);
    $results = $results->sortByDesc('created_at');
    return response()->json(['results'=>$results->values()->all()]);
  }

}
