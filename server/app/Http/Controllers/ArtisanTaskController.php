<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArtisanTaskRequest;  //  into ValidatedRequest
use App\ArtisanTask;

class ArtisanTaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $artisanTask = ArtisanTask::get();
      return response()->json(['artisanTask'=>$artisanTask]);
    }

    public function get($id)
    {
      $artisanTask = ArtisanTask::find($id);
      return response()->json(['artisanTask'=>$artisanTask]);
    }


    public function store($id=NULL, ArtisanTaskRequest $req)
    {

      $artisanTask = ArtisanTask::firstOrNew(['id'=>$id]);

      if($req->has('intervento_id'))
        $artisanTask->intervento_id = $req->input('intervento_id');
      if($req->has('note'))
        $artisanTask->note = $req->input('note');
      $artisanTask->save();

      return response()->json(['artisanTask'=>$artisanTask]);
    }

    public function artisan($user_id){
      $artisan_tasks = ArtisanTask::with('intervention', 'intervention.claim', 'intervention.claim.category', 'intervention.claim.flat', 'intervention.claim.flat.building')
      ->whereHas('intervention', function ($query) use($user_id) {
          $query->where('artisan_id', '=', $user_id);
      })->orderBy('id','desc')->limit(200)->get();
      return response()->json(['artisan_tasks'=>$artisan_tasks]);
    }
    public function artisanFilterByStatus($user_id, $status){
      $artisan_tasks = ArtisanTask::with('intervention', 'intervention.claim', 'intervention.claim.category', 'intervention.claim.flat', 'intervention.claim.flat.building')
      ->whereHas('intervention', function ($query) use($user_id, $status) {
          $query->where('artisan_id', '=', $user_id)->where('completed', $status);
      })->orderBy('id','desc')->limit(200)->get();
      return response()->json(['artisan_tasks'=>$artisan_tasks]);
    }

    public function delete($id)
    {
      $artisanTask = ArtisanTask::findOrFail($id);
      $artisanTask->delete();
      return response()->json(['success'=>true]);
    }


}
