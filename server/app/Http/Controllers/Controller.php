<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function log($dir,$file,$data){

    	$basepath="./log";
    	if(!file_exists("$basepath/$dir"))
    		mkdir("$basepath/$dir");

  		$filename="$basepath/$dir/$file";

  		file_put_contents($filename, print_r($data,true));

    }


    protected function error($text){
    	if(empty($text))
    		return;
		$validator = Validator::make([], []); // Empty data and rules fields
		$validator->errors()->add('message', $text);
		throw new ValidationException($validator);    	
    }

}
