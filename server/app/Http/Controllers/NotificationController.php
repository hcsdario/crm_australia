<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NotificationRequest;  //  into ValidatedRequest
use App\Notification;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      return response()->json(['notifications'=>auth()->user()->notifications]);
    }

    public function get($id)
    {
      $notification = auth()->user()->notifications()->where('id',$id)->firstOrFail();
      return response()->json(['notification'=>$notification]);
    }
    public function read($id)
    {
      $notification = auth()->user()->notifications()->where('id',$id)->firstOrFail();
      $notification->markAsRead();
      return response()->json(['notification'=>$notification]);
    }
    public function readAll()
    {
      $notifications = auth()->user()->unreadNotifications;
      $notifications->markAsRead();
      return response()->json(['notifications'=>$notifications]);
    }
    public function unreaded()
    {
      return response()->json(['notifications'=>auth()->user()->unreadNotifications]);
    }
    public function fetch()
    {
      return response()->json(['notifications'=>auth()->user()->notifications()->limit(100)->get()]);
    }



    public function delete($id)
    {
      $notification = auth()->user()->notifications()->where('id',$id)->firstOrFail();
      $notification->delete();
      return response()->json(['success'=>true]);
    }


}
