<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use App\File;
use App\Http\Requests\ContractRequest;  //  into ValidatedRequest

class ContractController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $contracts = Contract::with(['user','flat','flat.building'])->orderBy('id','desc')->get();
      return response()->json(['contracts'=>$contracts]);
    }

    public function get($id)
    {
      $contract = Contract::with(['user','flat','flat.building','files'])->find($id);
      return response()->json(['contract'=>$contract]);
    }


    public function store($id=NULL, Request $req)
    {
      $contract = Contract::firstOrNew(['id'=>$id]);
      $contract->data = $req->input('data');
      $contract->flat_id = $req->input('flat_id');
      $contract->user_id = $req->input('user_id');
      $contract->operator_id = auth()->user()->id;
      $contract->save();

      if(!$id){
        $user = \App\User::findOrFail($contract->user_id);
        $user->flats()->attach($contract->flat_id);
      }

      return response()->json(['contract'=>$contract]);
    }
    public function uploadFiles($id, Request $req)
    {
      $contract = Contract::findOrFail($id);
      $files = $req->file('files');
      if (!$files || count($files)<1) return response('NO FILES', 400);  // BAD REQUEST

      foreach($files as $file){
        $up_file = File::upload($file);
        if (!$up_file)
           return response('INVALID FILE', 400);  // BAD REQUEST

        $up_file->save();
        $contract->files()->attach($up_file->id);
      }
      return response()->json(['contract'=>$contract, 'files'=>$contract->files()->get()]);
    }
    public function deleteFile($id, $file_id)
    {
      $contract = Contract::findOrFail($id);
      $file = File::findOrFail($file_id);

      $contract->files()->detach($file->id);
      $file->delete(); //TODO: rimuovere file dal server
      return response()->json(['contract'=>$contract]);
    }

    public function delete($id)
    {
      $contract = Contract::findOrFail($id);
      $contract->delete();
      return response()->json(['success'=>true]);
    }


}
