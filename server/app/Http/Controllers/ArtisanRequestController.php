<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArtisanRequestHttp;  //  into ValidatedRequest
use Illuminate\Support\Facades\Auth;
use App\ArtisanRequest;

class ArtisanRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('jwt.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $artisanRequest = ArtisanRequest::get();
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }

    public function relations()
    {

      $artisanRequest = ArtisanRequest::with(['evaluation','evaluation.claim','evaluation.claim.flat','evaluation.claim.user','evaluation.claim.category','evaluation.claim.room','evaluation.claim.status'])->where('artisans_requests.artisan_id',$id)->get();
      return response()->json(['artisanRequest'=>$artisanRequest]);

    }

    public function get($id)
    {
      $artisanRequest = ArtisanRequest::find($id);
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }

    public function get_relations($id)
    {
      $artisanRequest = ArtisanRequest::with(['evaluation','evaluation.claim','evaluation.claim.flat','evaluation.claim.flat.building','evaluation.claim.images','evaluation.claim.availables','evaluation.claim.user','evaluation.claim.user.images','evaluation.claim.category','evaluation.claim.problem','evaluation.claim.room','evaluation.claim.status'])->get()->find($id);
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }


    public function store($id=NULL, ArtisanRequestHttp $req)
    {

      $artisanRequest = ArtisanRequest::firstOrNew(['id'=>$id]);

      if($req->has('evaluation_id'))
        $artisanRequest->evaluation_id = $req->input('evaluation_id');
      if($req->has('artisan_id'))
        $artisanRequest->artisan_id = $req->input('artisan_id');
      if($req->has('accepted'))
        $artisanRequest->accepted = $req->input('accepted');
      if($req->has('preventivo'))
        $artisanRequest->preventivo= $req->input('preventivo');
      if($req->has('urgency'))
        $artisanRequest->urgency = $req->input('urgency');

      $artisanRequest->save();

      if(!$id || $req->input('preventivo'))
        \App\User::findOrFail($artisanRequest->artisan_id)->notify(new \App\Notifications\Notific_sendArtisanRequest($artisanRequest));

      return response()->json(['artisanRequest'=>$artisanRequest]);
    }

    public function accept($id) // richiesta accettata dall'artigiano
    {
      $artisanRequest = ArtisanRequest::with(['evaluation'])->where(['id'=>$id])->firstOrFail();
      // se è già stato accettato
      if ($artisanRequest->accepted == ArtisanRequest::$ACCEPTED) return response()->json(['artisanRequest'=>$artisanRequest]);

      $artisanRequest->accepted = ArtisanRequest::$ACCEPTED;
      $artisanRequest->save();

      $evaluation = $artisanRequest->evaluation;

      //Controlla se l'artigiano è l' unico invitato e quindi conferma automaticamente l'intervento
      if (count($evaluation->artisanRequest()->count()) == 1) {
        $claim = \App\Claim::findOrFail($evaluation->claim_id);
        $intervention = \App\Intervention::makeFromArtisanRequest($artisanRequest->id);
        if ($intervention){
          $claim->status_id = \App\Claim::$CONFERMATO;
          $claim->save();
          $artisanRequest->accepted = ArtisanRequest::$CONFIRMED;
          $artisanRequest->save();
          // notifica cliente intervento confermato
          \App\User::findOrFail($claim->user_id)->notify(new \App\Notifications\Notific_confirmedClaim($artisanRequest));
        }
        else return response()->json(['error' => 'No Intervention created'], 500);
      }
      //notifica agenzia che l'artigiano ha accettato
      \App\User::getAgency()->notify(new \App\Notifications\Notific_sendArtisanRequest($artisanRequest));
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }
    public function confirmFromAgency($id) // confermato dall'agenzia
    {
      $artisanRequest = ArtisanRequest::with(['evaluation'])->where(['id'=>$id])->firstOrFail();
      // se è stato accettato
      if ($artisanRequest->accepted == ArtisanRequest::$ACCEPTED){
        $artisanRequest->accepted = ArtisanRequest::$CONFIRMED;
        $artisanRequest->save();
        $claim = \App\Claim::findOrFail($evaluation->claim_id);

        $intervention = \App\Intervention::makeFromArtisanRequest($artisanRequest->id);
        if ($intervention){
          $claim->status_id = \App\Claim::$CONFERMATO;
          $claim->save();
          // notifica cliente intervento confermato
          \App\User::findOrFail($claim->user_id)->notify(new \App\Notifications\Notific_confirmedClaim($artisanRequest));
        }
        else return response()->json(['error' => 'No Intervention created'], 500);
      }
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }
    public function rejected($id)
    {
      $artisanRequest = ArtisanRequest::findOrFail($id);
      $artisanRequest->accepted = ArtisanRequest::$REJECTED;
      $artisanRequest->save();
      //notifica agenzia che l'artigiano ha rigiutato
      \App\User::getAgency()->notify(new \App\Notifications\Notific_sendArtisanRequest($artisanRequest));
      return response()->json(['artisanRequest'=>$artisanRequest]);
    }

    public function delete($id)
    {
      $artisanRequest = ArtisanRequest::findOrFail($id);
      $artisanRequest->delete();
      return response()->json(['success'=>true]);
    }
    public function artisan($user_id)
    {
        $art_req = \App\ArtisanRequest::with('evaluation','evaluation.claim','evaluation.claim','evaluation.claim.category','evaluation.claim.room','evaluation.claim.problem','evaluation.claim.status','evaluation.claim.flat', 'evaluation.claim.user', 'evaluation.claim.flat.building')
        ->where('artisan_id',$user_id)->orderBy('id','desc')->limit(200)->get();
        return response()->json(['artisan_request'=>$art_req]);
    }
    public function artisanFilterByStatus($user_id, $status)
    {
      $art_req = \App\ArtisanRequest::with('evaluation','evaluation.claim','evaluation.claim','evaluation.claim.category','evaluation.claim.room','evaluation.claim.problem','evaluation.claim.status','evaluation.claim.flat', 'evaluation.claim.user', 'evaluation.claim.flat.building')
      ->where('artisan_id',$user_id)->where('accepted', $status)->orderBy('id','desc')->limit(200)->get();
      return response()->json(['artisan_request'=>$art_req]);
    }

}
