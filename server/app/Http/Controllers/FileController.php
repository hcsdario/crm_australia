<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use App\Http\Requests\ImageAddRequest;  //  into ValidatedRequest
use App\Image;
use App\File;

class FileController extends Controller
{
  private $public_path = './public';

  public function get_thumb($path){
    $chunk = explode("_", $path);
    if (count($chunk) == 2){
      $size = explode("x",$chunk[1]);
      $width = $size[0];
      $height = explode(".",$size[1])[0];
      $token = $chunk[0];
      $image = Image::where('token', '=', $token)->firstOrFail();
      return response()->file($this->public_path.$image->thumb($width, $height));
    }else{
      return response()->setStatusCode(400); // BAD REQUEST;
    }

  }
  public function get_scale($path){
    $chunk = explode("_", $path);
    if (count($chunk) == 2){
      $size = explode("x",$chunk[1]);
      $width = $size[0];
      $height = explode(".",$size[1])[0];
      $toke = $chunk[0];
      $image = Image::where('token', '=', $token)->firstOrFail();
      return response()->file($this->public_path.$image->scale($width, $height));
    }else{
      return response()->setStatusCode(400); // BAD REQUEST;
    }

  }
  public function download($id){
    $file = File::findOrFail($id);
    return response()->download($file->path(), $file->name);
  }


  public function delete($id,$file_id)
  {
    File::find($file_id)->delete();
    Image::find($id)->delete();
  }

}
