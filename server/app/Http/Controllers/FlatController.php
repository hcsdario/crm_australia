<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flat;
use App\Image;

class FlatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return response()->json(['flats'=>Flat::with(['users', 'image'])->get()]);
    }
    public function get($id)
    {
        $flat = Flat::with(['users', 'building', 'image'])->where(['id'=>$id])->firstOrFail();

        return response()->json(['flat'=>$flat]);
    }


    public function store($id=NULL, Request $req)
    {
      $flat = Flat::firstOrNew(['id'=>$id]);

      $flat->code = $req->input('code');
      $flat->floor = $req->input('floor');
      $flat->rooms_num = $req->input('rooms_num');
      $flat->mq = $req->input('mq');
      $flat->scale = $req->input('scale');
      $flat->internal = $req->input('internal');
      $flat->building_id = $req->input('building_id');
      $flat->save();

      $flat = Flat::with(['users', 'building', 'image'])->find($flat->id);
      return response()->json(['flat'=>$flat]);
    }

    public function addUser($id, $user_id)
    {
      $flat = Flat::findOrFail($id);
      $flat->users()->attach($user_id);
      $flat->save();
      return response()->json(['flat'=>$flat]);
    }
    public function removeUser($id, $user_id)
    {
      $flat = Flat::findOrFail($id);
      $flat->users()->detach($user_id);
      $flat->save();
      return response()->json(['flat'=>$flat]);
    }

    public function delete($id)
    {
      $flat = Flat::find($id);
      $flat->delete();
      return response()->json(['success'=>true,'id' => $id]);
    }
    public function addImage($id, Request $req)
    {
      $flat = Flat::find($id);
      $image = NULL;
      if ($req->hasFile('image')){
        $image = Image::upload($req->file('image'));
        $flat->image_id = $image->id;
        $flat->save();
      }
      return response()->json(['flat'=>$flat, 'image'=>$image]);
    }
    public function removeImage($id)
    {
      $flat = Flat::find($id);
      $flat->image_id = NULL;
      $flat->save();
    }

    public function search(Request $req)
    {
        $params =  $req->all();
        $query = Flat::with(['image', 'building'])->orderBy('id','desc');

        foreach ($params as $param => $value) {
          if (!$value) continue;
          switch ($param) {
            case 'user_id':
              $query = $query->whereHas('users', function($q) use($value) {
                $q->where('users.id', $value);
              });
              break;

            default:
              $query = $query->where($param, $value);
              break;
          }
        }
        $users = $query->limit(50)->get();
        return response()->json(['flats'=>$users, 'query'=>$params]);
    }
}
