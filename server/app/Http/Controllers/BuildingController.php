<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use App\Http\Requests\ImageAddRequest;  //  into ValidatedRequest
use App\Image;
use Illuminate\Support\Facades\DB;

class BuildingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return response()->json(['buildings'=>Building::with(['image', 'user'])->get()]);
    }
    public function get($id)
    {
        $building = Building::with(['flats', 'image', 'user'])->find($id);

        return response()->json(['building'=>$building]);
    }

    public function store($id=NULL, Request $req)
    {
      $building = Building::firstOrNew(['id'=>$id]);
      $building->code =  $req->input('code');
      $building->name =  $req->input('name');
      $building->user_id = $req->input('user_id'); // QUESTION: c'è un propietario?
      $building->zipCode = $req->input('zipCode');
      $building->state =  $req->input('state');
      $building->country =  $req->input('country');
      $building->city =  $req->input('city');
      $building->address =  $req->input('address');
      $building->longitude =  $req->input('longitude');
      $building->latitude =  $req->input('latitude');
      $building->save();

      return response()->json(['building'=>$building]);
    }

    public function delete($id)
    {
      $building = Building::findOrFail($id);
      $building->delete();
      return response()->json(['success'=>true,'id' => $id]);
    }
    public function addImage($id, ImageAddRequest $req)
    {
      $building = Building::find($id);
      $image = NULL;
      if ($req->hasFile('image')){
        $image = Image::upload($req->file('image'));
        $building->image_id = $image->id;
        $building->save();
      }
      return response()->json(['building'=>$building, 'image'=>$image]);
    }

    public function getFlats($id){
        $building = Building::with(['flats', 'flats.users', 'flats.image', 'flats.building'])->where('id', $id)->firstOrFail();
      return response()->json(['building'=>$building, 'flats'=>$building->flats]);
    }

    public function getBuildingsFlats(){
      $flats = DB::select("SELECT flats.* , buildings.zipCode,buildings.state,buildings.country,buildings.address,buildings.code as buildingCode FROM `flats`
      INNER JOIN buildings ON buildings.id=flats.building_id
      WHERE ISNULL(flats.deleted_at)  
      ORDER BY building_id ASC");
      return response()->json(['flats'=>$flats]);
    }

    public function removeImage($id)
    {
      $building = Building::find($id);
      $building->image_id = NULL;
      $building->save();
    }
}
