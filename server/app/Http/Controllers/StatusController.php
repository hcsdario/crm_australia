<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Http\Requests\CategoryRequest;  //  into ValidatedRequest

class StatusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $status = Status::get();
      return response()->json(['status'=>$status]);
    }

    public function get($id)
    {
      $status = Status::find($id);
      return response()->json(['status'=>$status]);
    }


    public function store($id=NULL, CategoryRequest $req)
    {

      $status = Status::firstOrNew(['id'=>$id]);
      $status->name = $req->input('name');
      $status->save();

      return response()->json(['status'=>$status]);
    }

    public function delete($id)
    {
      $status = Status::findOrFail($id);
      $status->delete();
      return response()->json(['success'=>true]);
    }

    
}
