<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Category;
use App\Room;
use App\Problem;
use App\Available;
use App\Http\Requests\ProblemRequest;  //  into ValidatedRequest

class ProblemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('jwt.auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $problem = Problem::get();
      return response()->json(['problem'=>$problem]);
    }

    public function get($id)
    {
      $problem = Problem::find($id);
      return response()->json(['problem'=>$problem]);
    }

    public function ofCategRoom($category_id, $room_id){
      $problems = Problem::where('category_id',$category_id)->where('room_id', $room_id)->get();

      return response()->json(['problems'=>$problems]);
    }


    public function store($id=NULL, ProblemRequest $req)
    {

      $problem = Problem::firstOrNew(['id'=>$id]);
      $problem->name = $req->input('name');
      $problem->category_id = $req->input('category_id');
      $problem->room_id = $req->input('room_id');
      $problem->save();

      return response()->json(['problem'=>$problem]);
    }

    public function delete($id)
    {
      $problem = Problem::findOrFail($id);
      $problem->delete();
      return response()->json(['success'=>true]);
    }


}
