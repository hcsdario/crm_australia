<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProposedDateRequest;  //  into ValidatedRequest
use App\ProposedDate;

class ProposedDateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function all()
    {
      $proposedDate = ProposedDate::get();
      return response()->json(['proposedDate'=>$proposedDate]);
    }

    public function get($id)
    {
      $artisanTask = ProposedDate::find($id);
      return response()->json(['proposedDate'=>$proposedDate]);
    }


    public function store($id=NULL, ProposedDateRequest $req)
    {

      $proposedDate = ProposedDate::firstOrNew(['id'=>$id]);

      if($req->has('artisan_request_id'))
        $proposedDate->artisan_request_id = $req->input('artisan_request_id');
      if($req->has('startAt'))
        $proposedDate->startAt = $req->input('startAt');
      if($req->has('endAt'))
        $proposedDate->endAt = $req->input('endAt');
      $proposedDate->save();

      return response()->json(['proposedDate'=>$proposedDate]);
    }

    public function delete($id)
    {
      $proposedDate = ProposedDate::findOrFail($id);
      $proposedDate->delete();
      return response()->json(['success'=>true]);
    }


}
