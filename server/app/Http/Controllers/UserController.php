<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ImageAddRequest;
use App\User;
use App\Image;
use App\File;
use App\infoArtisan;
use App\infoCustomer;
use App\infoOperator;
use Illuminate\Support\Facades\DB;
use App\Notifications\Notific;



class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('jwt.auth')->except('all');

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $users = User::with(['flats','images'])->get();
        return response()->json(['users'=>$users]);
    }
    public function artisans()
    {
        $artisans = User::with(['images'])->where(['type'=>User::$ARTISAN])->get();
        $artisans->load('info');
        return response()->json(['users'=>$artisans]);
    }
    public function customers()
    {
        $customers= User::with(['images','flats','flats.building'])->where(['type'=>User::$CUSTOMER])->get();
        $customers->load('info');
        return response()->json(['users'=>$customers]);
    }

    public function get($id)
    {
        $user = User::with(['flats','flats.building', 'images', 'claims', 'claims.category','claims.problem','claims.room','interventions'])->find($id);
        if (!$user)
          return response()->json(['error'=>"Not Found"], 404); // NOT FOUND

        $user->load('info');

        if ($user->type==USER::$ARTISAN)
          $user->load('info.categories');

        return response()->json(['user'=>$user]);
    }

    public function getFlats($id){
      $user = User::findOrFail($id);
      return response()->json(['user'=>$user, 'flats'=>$user->flats()]);
    }

    public function getOperators()
    {
        $user = User::where(['type'=>0])->get();
        return response()->json(['user'=>$user]);
    }


    public function getArtisanRelations($id)
    {
        $user = User::with(['images'])->find($id);
        if (!$user) return response()->json(['error'=>"Not Found"], 404); // NOT FOUND

        $user->load('info');

        return response()->json(['user'=>$user]);
    }

    public function activeClaims($id){
      $count=DB::table("claims")->where(["user_id" => $id])->count();
      return response()->json(['count'=>$count]);
    }

    public function updateDeviceToken($id, Request $req){
      $user = User::findOrFail($id);
      $user->device_token = $req->input('token');
      $user->save();
      return response()->json(['user'=>$user]);
    }

    protected function generate(UserRequest $req) // TODO solo admin può, aggiungere controllo in Request
    {
        $psw = str_random(8);
        $type = User::$CUSTOMER;

        if ($req->has('type') && $req->input('type')==2) $type = User::$ARTISAN;

        $user =  User::create([
            'name' => $req['name'],
            'surname' => $req['surname'],
            'email' => $req['email'],
            'password' => bcrypt($psw),
            'type' => $type,
            'admin' => 0
        ]);

        if ($req->input('info'))
        {
          if ($type == User::$ARTISAN){
            $this->saveInfoArtisan($user, $req->input('info'));
          }
          else $this->saveInfoCustomer($user, $req->input('info'));
        }

        $user['clear_password'] = $psw;
        // notifica creazione account
        $user->notify(new \App\Notifications\Notific_newUser($user));
        return response()->json(['user'=>$user]);
    }

    public function store($id=NULL, UserRequest $req)
    {
      $user = User::findOrFail($id);
      if ($req->has('name'))
     	 $user->name = $req->input('name');
      if ($req->has('surname'))
     	 $user->surname = $req->input('surname');
      if ($req->has('email'))
    	  $user->email = $req->input('email');

      if ($req->has('password'))
        $user->password = $req->input('password');

      if ($req->has('info')){
        switch ($user->type) {
          case USER::$ARTISAN:
              $this->saveInfoArtisan($user, $req->input('info'));
            break;
          case USER::$CUSTOMER:
              $this->saveInfoCustomer($user, $req->input('info'));
            break;
          case USER::$OPERATOR:
            $this->saveInfoOperator($user, $req->input('info'));
            break;
        }
      }

      $user->save();
      $user->load('info');

      return response()->json(['user'=>$user]);
    }


    public function delete($id)
    {
      $user = User::findOrFail($id);
      $user->delete();
      return response()->json(['success'=>true]);
    }

        public function addImage($id, ImageAddRequest $req)
    {
      $user = User::findOrFail($id);
      $image = NULL;
      if ($req->hasFile('image')){
        $image = Image::upload($req->file('image'));

        if (!$image)
          return response()->setStatusCode(400);  // BAD REQUEST

        $image->save();
        $user->images()->attach($image->id);
      }
      return response()->json(['user'=>$user, 'images'=>$user->images]);
    }

    public function removeImage($id, $image_id)
    {
      $user = User::find($id);
      $user->images()->detach($image_id);
      $image= Image::findOrFail($image_id)->delete();
      $user->save();
      return response()->json(['user'=>$user, 'image'=>$user->images]);
    }

    public function search(Request $req)
    {
        $params =  $req->all();
        $query = User::with(['images','flats'])->orderBy('users.id','desc');
        foreach ($params as $param => $value) {
          switch ($param) {

            case 'name':
              $query = $query-> where(function ($query) use($value) {
                  $query->orWhere('surname', 'like', '%'.$value.'%')->orWhere('name', 'like', '%'.$value.'%');
              });
              break;

            case 'category_id':
              $query = $query->whereHas('info_artisan.categories', function($q) use($value) {
                $q->where('artisans_categories.artisan_category_id', $value);
              });
              break;

            case 'no_flat':
              $query = $query->has('flats', '>', 0);
              break;

            default:
              $query = $query->where($param, $value);
              break;
          }
        }
        $users = $query->limit(50)->get();
        return response()->json(['users'=>$users, 'query'=>$params]);
    }

    public function searchCustomerWithFlats(Request $req)
    {
        $params =  $req->all();
        $query = User::with(['images','flats'])->orderBy('id','desc');
        foreach ($params as $param => $value) {
          switch ($param) {
            case 'name':
              $query = $query-> where(function ($query) use($value) {
                  $query->orWhere('surname', 'like', '%'.$value.'%')->orWhere('name', 'like', '%'.$value.'%');
              });
              break;
            case 'category_id':
              $query = $query->whereHas('info_artisan.categories', function($q) use($value) {
                $q->where('artisans_categories.artisan_category_id', $value);
              });
              break;

            default:
              $query = $query->where($param, $value);
              break;
          }
        }
        $users = $query->limit(50)->get();
        return response()->json(['users'=>$users, 'query'=>$params]);
    }



    // UTILITY NOT IN ROUTES

    public function saveInfoArtisan($user, $req_info){
        $info = infoArtisan::firstOrNew(['user_id'=>$user->id]);

        $info->user_id = $user->id;
        if (@$req_info['telephone'])
          $info->telephone = $req_info['telephone'];
        if (@$req_info['address'])
          $info->address = $req_info['address'];
        if (@$req_info['piva'])
          $info->piva = $req_info['piva'];
        if (@$req_info['identificativo_aziendale'])
          $info->identificativo_aziendale = $req_info['identificativo_aziendale'];

        $info->save();
        if (@$req_info['categories']){
          $cats_ids = [];
          foreach ($req_info['categories'] as $cat) array_push($cats_ids,$cat['id']);
          $info->categories()->sync($cats_ids);
        }

        $info->save();
        return true;
    }
    public function saveInfoCustomer($user, $req_info){
        $info = infoCustomer::firstOrNew(['user_id'=>$user->id]);
        $info->user_id = $user->id;
        if (@$req_info['telephone'])
          $info->telephone = $req_info['telephone'];
        if (@$req_info['address'])
          $info->address = $req_info['address'];
        $user->info()->save($info);
        return true;
    }
    public function saveInfoOperator($user, $req_info){
        $info = infoOperator::firstOrNew(['user_id'=>$user->id]);
        if (@$req_info['telephone'])
          $info->telephone = $req_info['telephone'];
        if (@$req_info['address'])
          $info->address = $req_info['address'];

        $user->info()->save($info);
        return true;
    }
    public function testMail(){
      $user = User::findOrFail(3);
      $user->notify(new Notific($user));
      return 'aaa';
    }

}
