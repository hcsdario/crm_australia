<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\Http\Requests\TodoRequest;  //  into ValidatedRequest

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all($user_id)
    {
        return response()->json(['todo'=>Todo::orderBy("created_at","desc")->get()]);
    }
    public function get($id)
    {
        $todo = Todo::findOrFail($id);

        return response()->json(['todo'=>$todo]);
    }

    public function store($id=NULL, TodoRequest $req)
    {

      $todo = Todo::firstOrNew(['id'=>$id]);
      $todo->user_id = $req->input('user_id');
      $todo->assigned_user_id = $req->input('assigned_user_id');
      $todo->assigned_user_label = $req->input('assigned_user_label');
      $todo->note = $req->input('note');
      $todo->date =  $req->input('date');
      $todo->startAt =  date("Y/m/d",strtotime($req->input('date')));
      $todo->endAt =  date("Y/m/d",strtotime($req->input('date')));
      if($req->has('completed'))
	      $todo->completed =  $req->input('completed');
      $todo->save();

      return response()->json(['todo'=>$todo]);
    }
    public function completed($id)
    {
      $todo = Todo::firstOrNew(['id'=>$id]);
      $todo->completed = 1;
      $todo->save();

      return response()->json(['todo'=>$todo]);
    }

    public function delete($id)
    {
      $todo = Todo::findOrFail($id);
      $todo->delete();
      return response()->json(['success'=>true]);
    }

}
