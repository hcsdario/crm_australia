<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Claim extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'note',
        'category_id',
        'room_id',
        'problem_id',
        'user_id',
        'flat_id',
        'completed'
    ];

    // id riferiti a claims_status
    public static $IN_ATTESA_VISUALIZZAZIONE = 0;
    public static $VISUALIZZATO = 1;
    public static $CONFERMATO = 2;
    public static $RISCHEDULATO = 3;
    public static $COMPLETATO = 4;
    public static $ANNULLATO = 5;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function flat()
    {
        return $this->belongsTo('App\Flat');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function evaluation()
    {
        return $this->hasOne('App\Evaluation');
    }

    public function images(){
      // belongsToMany molti a molti con tabella in mezzo 2 chiavi esterne
      return $this->belongsToMany('App\Image', 'claims_images', 'claim_id', 'image_id');
    }

    public function category(){
      // belongsToMany molti a molti con tabella in mezzo 2 chiavi esterne
      return $this->belongsTo('App\Category', 'category_id');
    }

    public function room(){
      //singola chiave esterna
      return $this->belongsTo('App\Room', 'room_id');
    }

    public function problem(){
      return $this->belongsTo('App\Problem', 'problem_id');
    }

    public function status(){
      return $this->belongsTo('App\Status');
    }

    public function availables(){
      // la sua chiave è richiamata più volte in un altra tabella
      return $this->hasMany('App\Available','claim_id');
    }

    public function interventions(){
      // la sua chiave è richiamata più volte in un altra tabella
      return $this->hasMany('App\Intervention','claim_id');
    }


}
