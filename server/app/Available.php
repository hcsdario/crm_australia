<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Available extends Model
{
    protected $table="availabilities";

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'startAt',
        'rangeStart',
        'endAt',
        'rangeEnd',
        'claim_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function claim(){
      return $this->belongsTo('App\Claim', 'claim_id', 'id');
    }

}
