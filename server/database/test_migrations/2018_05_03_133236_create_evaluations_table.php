<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operator_id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('claim_id');
            $table->unsignedInteger('intervention_id')->nullable();
            $table->mediumText('note')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('operator_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('claim_id')->references('id')->on('claims');
            //$table->foreign('intervention_id')->references('id')->on('interventions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
