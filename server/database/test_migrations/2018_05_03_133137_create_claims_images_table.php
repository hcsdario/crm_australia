<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims_images', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('claim_id');
          $table->unsignedInteger('image_id');
          $table->timestamps();
          
          $table->foreign('claim_id')->references('id')->on('claims');
          $table->foreign('image_id')->references('id')->on('files');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims_images');
    }
}
