<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// pid : 37092 | port: 8002

Route::get('/', function () {
  $app = app();
  $routes = $app->routes->getRoutes();
  return view ('list_routes',compact('routes'));
});

Route::get('/docs', function () {
  return \File::get(public_path() . '/docs/index.html');
});

Route::get('preview_mail', function() {
  $user = \App\User::where('email','matteo@hcslab.it')->firstOrFail();
  $user->clear_password = str_random(8);
  $message =  (new \App\Notifications\Notific_newUser($user))->toMail($user);
  $markdown = new \Illuminate\Mail\Markdown(view(), ['mails.newUser']);
  return $markdown->render('mails.newUser',$message->data());
});

Route::get('test_notific', function() {
  $user = \App\User::where('email','matteo@hcslab.it')->firstOrFail();
  $user->email = 'matteobarato95@gmail.com';
  $user->clear_password = str_random(8);
  $user->notify(new \App\Notifications\Notific_newUser($user));
  return response()->json(['notification'=>$user->notifications, 'user'=>$user]);
});

Route::middleware(['logger'])->group(function () {
  Auth::routes();
});

Route::middleware(['jwt.auth', 'logger'])->group(function () {

  Route::get('users', 'UserController@all')->name('all user');
  Route::get('users/artisans', 'UserController@artisans')->name('all artisans');
  Route::get('users/customers', 'UserController@customers')->name('all customers');
  Route::get('users/operators', 'UserController@getOperators')->name('all operators');
  Route::get('user/{id}', 'UserController@get')->name('info user');
  Route::get('user/{id}/artisan_request', 'ArtisanRequestController@artisan')->name('all artisan request relations');
  Route::get('user/{id}/artisan_request/status/{status}', 'ArtisanRequestController@artisanFilterByStatus')->name('all artisan request relations filtered by status (accepted)');
  Route::get('user/{id}/interventions', 'InterventionController@artisan')->name('all artisan\'s interventions');
  Route::get('user/{id}/interventions/status/{status}', 'InterventionController@artisanFilterByStatus')->name('all artisan request relations filtered by status (accepted)');
  Route::get('user/{id}/artisan_tasks', 'ArtisanTaskController@artisan')->name('all artisan\'s interventions');
  Route::get('user/{id}/artisan_tasks/status/{status}', 'ArtisanTaskController@artisanFilterByStatus')->name('all artisan\'s interventions');
  Route::get('user/{id}/flats', 'UserController@get')->name('get all flats of user');
  Route::get('user/{id}/artisan/rel', 'UserController@getArtisanRelations')->name('get user with all artisan relations');
  Route::post('user/{id}/edit', 'UserController@store')->name('edit user');
  Route::post('user/{id}/image/add', 'UserController@addImage')->name('add image to user');
  Route::get('user/{id}/image/{image_id}/remove', 'UserController@removeImage')->name('remove image from user');
  Route::post('user/{id}/cleanimage', 'UserController@cleanimage')->name('remove all image from user');
  Route::post('user/{id}/update_device_token', 'UserController@updateDeviceToken')->name('update device token for FCM push notification');
  Route::get('user/{id}/delete', 'UserController@delete')->name('delete user');
  Route::post('users/search', 'UserController@search')->name('search user');
  Route::post('user/generate', 'UserController@generate')->name('generate new user');
  Route::get('user/{id}/activeClaims', 'UserController@activeClaims')->name('active user claims');

  /* BUILDINGS */
  Route::get('buildings', 'BuildingController@all')->name('all buildings');
  Route::get('building/{id}', 'BuildingController@get')->name('info building');
  Route::post('building/create', 'BuildingController@store')->name('create building');
  Route::post('building/{id}/edit', 'BuildingController@store')->name('edit building');
  Route::get('building/delete/{id}', 'BuildingController@delete')->name('delete building');

  Route::get('buildings/flats', 'BuildingController@getBuildingsFlats')->name('flats of buildings');
  Route::get('building/{id}/flats', 'BuildingController@getFlats')->name('flats of building');
  Route::post('building/{id}/image/add', 'BuildingController@addImage')->name('! add image to building');
  Route::get('building/{id}/image/{image_id}/remove', 'BuildingController@removeImage')->name('remove image from building');
  Route::get('building/{id}/delete', 'BuildingController@delete')->name('delete building');

  /* FLATS */
  Route::get('flats', 'FlatController@all')->name('all flats');
  Route::get('flat/{id}', 'FlatController@get')->name('info flat');
  Route::get('flat/{id}/user/{user_id}/add', 'FlatController@addUser')->name('add user into flat');
  Route::get('flat/{id}/user/{user_id}/remove', 'FlatController@removeUser')->name('add user into flat');
  Route::post('flat/create', 'FlatController@store')->name('create flat');
  Route::post('flat/{id}/edit', 'FlatController@store')->name('edit flat');
  Route::post('flat/{id}/image/add', 'FlatController@addImage')->name('add image to flat');
  Route::get('flat/{id}/image/{image_id}/remove', 'FlatController@removeImage')->name('remove image from flat');
  Route::get('flat/{id}/delete', 'FlatController@delete')->name('delete flat');
  Route::post('flats/search', 'FlatController@search')->name('search flat');

  /* CLAIMS */
  Route::get('claims', 'ClaimController@all')->name('all claims');
  Route::post('claims/search', 'ClaimController@search')->name('search claims');
  Route::get('claims/open', 'ClaimController@claimsOpen')->name('claims open');
  Route::get('claims/relations', 'ClaimController@all_relations')->name('all claims rel');
  Route::get('claims/user/{id}', 'ClaimController@user_claims')->name('user claims');
  Route::get('claim/{id}', 'ClaimController@get')->name('info claim');
  Route::get('claim/rel/{id}', 'ClaimController@get_relations')->name('info claim rel');


  Route::post('claim/create', 'ClaimController@store')->name('create claim');


  Route::post('claim/{id}/edit', 'ClaimController@store')->name('edit claim');
  Route::post('claim/{id}/image/add', 'ClaimController@addImage')->name('add image to claim');
  Route::get('claim/{id}/image/{image_id}/remove', 'ClaimController@removeImage')->name('remove image from claim');
  Route::get('claim/{id}/delete', 'ClaimController@delete')->name('delete claim');


  /* CATEGORY */
  Route::get('categories', 'CategoryController@all')->name('all category');
  Route::get('category/{id}', 'CategoryController@get')->name('info category');
  Route::post('category/create', 'CategoryController@store')->name('create category');
  Route::post('category/{id}/edit', 'CategoryController@store')->name('edit category');
  Route::get('category/{id}/delete', 'CategoryController@delete')->name('delete category');

  /* ROOM */
  Route::get('rooms', 'RoomController@all')->name('all room');
  Route::get('room/{id}', 'RoomController@get')->name('info room');
  Route::post('room/create', 'RoomController@store')->name('create room');
  Route::post('room/{id}/edit', 'RoomController@store')->name('edit room');
  Route::get('room/{id}/delete', 'RoomController@delete')->name('delete room');

  /* PROBLEM */
  Route::get('problems', 'ProblemController@all')->name('all problem');
  Route::get('problems/of/{category_id}/{room_id}', 'ProblemController@ofCategRoom')->name('all problem');
  Route::get('problem/{id}', 'ProblemController@get')->name('info problem');
  Route::post('problem/create', 'ProblemController@store')->name('create problem');
  Route::post('problem/{id}/edit', 'ProblemController@store')->name('edit problem');
  Route::get('problem/{id}/delete', 'ProblemController@delete')->name('delete problem');
  /* PROBLEM */
  Route::get('problems', 'ProblemController@all')->name('all problem');
  Route::get('problem/{id}', 'ProblemController@get')->name('info problem');
  Route::post('problem/create', 'ProblemController@store')->name('create problem');
  Route::post('problem/{id}/edit', 'ProblemController@store')->name('edit problem');
  Route::get('problem/{id}/delete', 'ProblemController@delete')->name('delete problem');


  /* ARTISAN TASK */
  Route::get('artisanTasks', 'ArtisanTaskController@all')->name('all artisan task');
  Route::get('artisanTask/{id}', 'ArtisanTaskController@get')->name('info artisan task');
  Route::post('artisanTask/create', 'ArtisanTaskController@store')->name('create artisan task');
  Route::post('artisanTask/{id}/edit', 'ArtisanTaskController@store')->name('edit artisan task');
  Route::get('artisanTask/{id}/delete', 'ArtisanTaskController@delete')->name('delete artisan task');


  /* ARTISAN REQUEST */
  Route::get('artisanRequests', 'ArtisanRequestController@all')->name('all artisan request');
  Route::get('artisanRequests/relations', 'ArtisanRequestController@relations')->name('all artisan request relations');
  Route::get('artisanRequests/relations/{id}', 'ArtisanRequestController@get_relations')->name('all artisan request relations');
  Route::get('artisanRequest/{id}', 'ArtisanRequestController@get')->name('info artisan request');
  Route::get('artisanRequest/{id}/relations', 'ArtisanRequestController@get_relations')->name('get artisan request relations');
  Route::post('artisanRequest/edit', 'ArtisanRequestController@store')->name('create artisan request');
  Route::post('artisanRequest/{id}/edit', 'ArtisanRequestController@store')->name('edit artisan request');
  Route::get('artisanRequest/{id}/delete', 'ArtisanRequestController@delete')->name('delete artisan request');
  Route::get('artisanRequest/{id}/accept', 'ArtisanRequestController@accept')->name('accept artisan request');
  Route::get('artisanRequest/{id}/confirm_from_agency', 'ArtisanRequestController@confirmFromAgency')->name('accept artisan confirmed from agency');
  Route::get('artisanRequest/{id}/reject', 'ArtisanRequestController@reject')->name('reject artisan request');

  /* Intervention */
  Route::get('interventions', 'InterventionController@all')->name('all intervention');
  Route::get('intervention/{id}', 'InterventionController@get')->name('info intervention');
  Route::get('intervention/{id}/relations', 'InterventionController@get_relations')->name('info intervention');
  Route::post('intervention/create', 'InterventionController@store')->name('create intervention');
  Route::post('intervention/{id}/edit', 'InterventionController@store')->name('edit intervention');
  Route::get('intervention/{id}/completed', 'InterventionController@completed')->name('check as completed the intervention');
  Route::get('intervention/{id}/delete', 'InterventionController@delete')->name('delete intervention');

  /* Evaluation */
  Route::get('evaluation', 'EvaluationController@all')->name('all evaluation');
  Route::get('evaluation/{id}', 'EvaluationController@get')->name('info evaluation');
  Route::get('evaluation/{id}/relations', 'EvaluationController@get_relations')->name('get evaluation relations');
  Route::post('evaluation/create', 'EvaluationController@store')->name('create evaluation');
  Route::post('evaluation/{id}/edit', 'EvaluationController@store')->name('edit evaluation');
  Route::get('evaluation/{id}/delete', 'EvaluationController@delete')->name('delete evaluation');

  /* ProposedDate */
  Route::get('proposedDates', 'ProposedDateController@all')->name('all proposed date');
  Route::get('proposedDate/{id}', 'ProposedDateController@get')->name('info proposed date');
  Route::post('proposedDate/create', 'ProposedDateController@store')->name('create proposed date');
  Route::post('proposedDate/{id}/edit', 'ProposedDateController@store')->name('edit proposed date');
  Route::get('proposedDate/{id}/delete', 'ProposedDateController@delete')->name('delete proposed date');

  /* Notification */
  Route::get('notifications', 'NotificationController@all')->name('all notification');
  Route::get('notifications/fetch', 'NotificationController@fetch')->name('notifications limit 100');
  Route::get('notifications/unreaded', 'NotificationController@unreaded')->name('unreaded notifications');
  Route::get('notifications/read_all', 'NotificationController@readAll')->name('read notification');
  Route::get('notification/{id}', 'NotificationController@get')->name('info notification');
  Route::get('notification/{id}/read', 'NotificationController@read')->name('read notification');
  Route::get('notification/{id}/delete', 'NotificationController@delete')->name('delete notification');

  /* notificationCategory */
  Route::get('notificationCategory', 'NotificationCategoryController@all')->name('all notification category');
  Route::get('notificationCategory/{id}', 'NotificationCategoryController@get')->name('info notification category');
  Route::get('notificationCategory/{id}/relations', 'NotificationCategoryController@get_relations')->name('relations notification category');
  Route::post('notificationCategory/create', 'NotificationCategoryController@store')->name('create notification category');
  Route::post('notificationCategory/{id}/edit', 'NotificationCategoryController@store')->name('edit notification category');
  Route::get('notificationCategory/{id}/delete', 'NotificationCategoryController@delete')->name('delete notification category');

  /* TODO */
  Route::get('/user/{user_id}/todo', 'TodoController@all')->name('get all todo of users');
  Route::get('todo/{id}', 'TodoController@get')->name('get todo');
  Route::post('todo/create', 'TodoController@store')->name('create todo');
  Route::post('todo/{id}/edit', 'TodoController@store')->name('edit todo');
  Route::get('todo/{id}/delete', 'TodoController@delete')->name('delete todo');
  Route::get('todo/{id}/completed', 'TodoController@completed')->name('set todo completed');

  /* ARTISAN CATEGORIES */
  Route::get('artisan_categories/all', 'ArtisanCategoryController@all')->name("get all artisan's categories");

  /*CONTRACT */
  Route::get('contracts', 'ContractController@all')->name('all contract');
  Route::get('contract/{id}', 'ContractController@get')->name('info contract');
  Route::get('contract/{id}/relations', 'ContractController@get_relations')->name('relations contract');
  Route::post('contract/create', 'ContractController@store')->name('create contract');
  Route::post('contract/{id}/edit', 'ContractController@store')->name('edit contract');
  Route::get('contract/{id}/delete', 'ContractController@delete')->name('delete contract');
  Route::post('contract/{id}/upload_files', 'ContractController@uploadFiles')->name('upload files for contract');
  Route::get('contract/{id}/file/{file_id}/delete', 'ContractController@deleteFile')->name('delete contract file');
});

/* IMAGE */
Route::get('images/thumb/{path}', 'FileController@get_thumb')->name('obtain thumb of image');
Route::get('images/scale/{path}', 'FileController@get_thumb')->name('obtain scaled image');
Route::get('file/{id}/download', 'FileController@download')->name('obtain scaled image');

Route::post('/search', 'SearchController@globalSearch')->name('search');


 Route::get('/dashboard', 'HomeController@index')->name('home');

 Route::get('/dashboardSources', 'HomeController@dashboardSources')->name('dashboardSources');

 Route::get('/dashboardSources/getLastClaims', 'HomeController@getLastClaims')->name('dashboardSources getLastClaims');
 Route::get('/dashboardSources/getLastClaims/{id}', 'HomeController@getLastClaims')->name('dashboardSources getLastClaims id');
 Route::get('/dashboardSources/graphsClaim', 'HomeController@graphsClaim')->name('dashboardSources graphsClaim id');
 Route::get('/dashboardSources/graphsClaim/{id}', 'HomeController@graphsClaim')->name('dashboardSources graphsClaim id');

 Route::get('/dashboardSources/getInterventions', 'HomeController@getInterventions')->name('dashboardSources getInterventions');
 Route::get('/dashboardSources/getInterventions/{id}', 'HomeController@getInterventions')->name('dashboardSources getInterventions id');

 Route::get('/dashboardSources/graphsPhases', 'HomeController@graphsPhases')->name('dashboardSources getInterventions');
 Route::get('/dashboardSources/graphsPhases/{id}', 'HomeController@graphsPhases')->name('dashboardSources getInterventions id');

 Route::get('/dashboardSources/calendarData', 'HomeController@calendarData')->name('dashboardSources calendar');
 Route::get('/dashboardSources/calendarData/{id}', 'HomeController@calendarData')->name('dashboardSources calendar id');
