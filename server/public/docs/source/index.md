---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://hellocreativestudio.net/myFlat/server/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_45def4da6d09e649f3b2c95189bbb020 -->
## Show the application&#039;s login form.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//login",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET login`

`HEAD login`


<!-- END_45def4da6d09e649f3b2c95189bbb020 -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## login

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//login" \
-H "Accept: application/json" \
    -d "email"="breitenberg.neva@example.net" \
    -d "password"="perferendis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//login",
    "method": "POST",
    "data": {
        "email": "breitenberg.neva@example.net",
        "password": "perferendis"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST login`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | email |  required  | Maximum: `255`
    password | string |  required  | Minimum: `6`

<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_e65925f23b9bc6b93d9356895f29f80c -->
## Log the user out of the application.

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//logout" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//logout",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST logout`


<!-- END_e65925f23b9bc6b93d9356895f29f80c -->

<!-- START_d7e8ee2d51ff436e319caca5ab309cd9 -->
## Show the application registration form.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//register" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//register",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET register`

`HEAD register`


<!-- END_d7e8ee2d51ff436e319caca5ab309cd9 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## register

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//register" \
-H "Accept: application/json" \
    -d "name"="rerum" \
    -d "surname"="rerum" \
    -d "email"="ebeier@example.net" \
    -d "admin"="242" \
    -d "type"="242" \
    -d "password"="rerum" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//register",
    "method": "POST",
    "data": {
        "name": "rerum",
        "surname": "rerum",
        "email": "ebeier@example.net",
        "admin": 242,
        "type": 242,
        "password": "rerum"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST register`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `255`
    surname | string |  required  | Maximum: `255`
    email | email |  required  | Maximum: `255`
    admin | numeric |  optional  | Maximum: `255`
    type | numeric |  required  | Maximum: `255`
    password | string |  required  | Minimum: `6`

<!-- END_d7aad7b5ac127700500280d511a3db01 -->

<!-- START_f9bb43b2d406a133a7646f806a34310b -->
## Display the form to request a password reset link.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//password/reset" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//password/reset",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET password/reset`

`HEAD password/reset`


<!-- END_f9bb43b2d406a133a7646f806a34310b -->

<!-- START_feb40f06a93c80d742181b6ffb6b734e -->
## Send a reset link to the given user.

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//password/email" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//password/email",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST password/email`


<!-- END_feb40f06a93c80d742181b6ffb6b734e -->

<!-- START_5a0014b83f352dff4e16558b63bfd23e -->
## Display the password reset view for the given token.

If no token is present, display the link request form.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//password/reset/{token}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//password/reset/{token}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET password/reset/{token}`

`HEAD password/reset/{token}`


<!-- END_5a0014b83f352dff4e16558b63bfd23e -->

<!-- START_cafb407b7a846b31491f97719bb15aef -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//password/reset" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//password/reset",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST password/reset`


<!-- END_cafb407b7a846b31491f97719bb15aef -->

<!-- START_00160fc4c9a4c1a8b75f8f743c6192fa -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//users",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "users": [
        {
            "id": 2,
            "name": "abbani",
            "surname": "inabbi",
            "type": "1",
            "admin": 0,
            "email": "matteo@gmail.com",
            "deleted_at": null,
            "created_at": "2018-05-11 13:23:04",
            "updated_at": "2018-05-11 13:23:04",
            "flat": null,
            "images": []
        },
        {
            "id": 3,
            "name": "Matteo",
            "surname": "Barato",
            "type": "0",
            "admin": 1,
            "email": "matteo@hcslab.it",
            "deleted_at": null,
            "created_at": "2018-05-14 13:53:58",
            "updated_at": "2018-05-14 13:53:58",
            "flat": {
                "id": 2,
                "building_id": 1,
                "image_id": null,
                "user_id": 3,
                "internal": "13A",
                "scale": "2C",
                "created_at": "2018-05-08 11:47:37",
                "updated_at": "2018-05-08 11:47:37",
                "deleted_at": null
            },
            "images": []
        },
        {
            "id": 8,
            "name": "Elettrico Ticino",
            "surname": "nasitra",
            "type": "2",
            "admin": 0,
            "email": "gdjhsa@gmail.com",
            "deleted_at": null,
            "created_at": "2018-05-23 09:04:02",
            "updated_at": "2018-05-23 12:56:43",
            "flat": null,
            "images": [
                {
                    "id": 17,
                    "token": "http:\/\/hellocreativestudio.net\/myFlat\/server2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT",
                    "50w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_100x100.jpg",
                    "100w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_100x100.jpg",
                    "300w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_300x300.jpg",
                    "1000w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_1000x1000.jpg",
                    "original": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/original\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT.jpg"
                }
            ]
        }
    ]
}
```

### HTTP Request
`GET users`

`HEAD users`


<!-- END_00160fc4c9a4c1a8b75f8f743c6192fa -->

<!-- START_62bf5161b54e6d9f99f99f57fc7c49b6 -->
## users/artisans

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//users/artisans" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//users/artisans",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "users": [
        {
            "id": 8,
            "name": "Elettrico Ticino",
            "surname": "nasitra",
            "type": "2",
            "admin": 0,
            "email": "gdjhsa@gmail.com",
            "deleted_at": null,
            "created_at": "2018-05-23 09:04:02",
            "updated_at": "2018-05-23 12:56:43",
            "images": [
                {
                    "id": 17,
                    "token": "http:\/\/hellocreativestudio.net\/myFlat\/server2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT",
                    "50w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_100x100.jpg",
                    "100w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_100x100.jpg",
                    "300w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_300x300.jpg",
                    "1000w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT_1000x1000.jpg",
                    "original": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/original\/2JoL2GfDuYFn0fV4uVCVUO4ERtv513rsv8mpjtdT.jpg"
                }
            ],
            "info": {
                "id": 1,
                "user_id": 8,
                "telephone": "783347",
                "address": "uyruiwyyi",
                "piva": 1234567,
                "created_at": "2018-05-23 09:04:02",
                "updated_at": "2018-05-23 09:04:02"
            }
        }
    ]
}
```

### HTTP Request
`GET users/artisans`

`HEAD users/artisans`


<!-- END_62bf5161b54e6d9f99f99f57fc7c49b6 -->

<!-- START_111698673d68f3c8a777debcf1bb257e -->
## user/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//user/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Not Found"
}
```

### HTTP Request
`GET user/{id}`

`HEAD user/{id}`


<!-- END_111698673d68f3c8a777debcf1bb257e -->

<!-- START_194646873298d9247a74352e517c1034 -->
## user/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//user/{id}/edit" \
-H "Accept: application/json" \
    -d "name"="nemo" \
    -d "surname"="nemo" \
    -d "passssword"="nemo" \
    -d "email"="tessie.kreiger@example.net" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/{id}/edit",
    "method": "POST",
    "data": {
        "name": "nemo",
        "surname": "nemo",
        "passssword": "nemo",
        "email": "tessie.kreiger@example.net"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST user/{id}/edit`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | Maximum: `255`
    surname | string |  optional  | Maximum: `255`
    passssword | string |  optional  | Maximum: `255`
    email | email |  optional  | Maximum: `255`

<!-- END_194646873298d9247a74352e517c1034 -->

<!-- START_7333404fdcf4b93ee96c3ebc6771f32b -->
## user/{id}/image/add

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//user/{id}/image/add" \
-H "Accept: application/json" \
    -d "image"="quia" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/{id}/image/add",
    "method": "POST",
    "data": {
        "image": "quia"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST user/{id}/image/add`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    image | image |  required  | Must be an image (jpeg, png, bmp, gif, or svg) Maximum: `10240  `

<!-- END_7333404fdcf4b93ee96c3ebc6771f32b -->

<!-- START_62027c07563ca21c78bdb0939c472601 -->
## user/{id}/image/{image_id}/remove

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//user/{id}/image/{image_id}/remove" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/{id}/image/{image_id}/remove",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET user/{id}/image/{image_id}/remove`

`HEAD user/{id}/image/{image_id}/remove`


<!-- END_62027c07563ca21c78bdb0939c472601 -->

<!-- START_65aca9b80e773bfc1906be12ac9f31fe -->
## user/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//user/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET user/{id}/delete`

`HEAD user/{id}/delete`


<!-- END_65aca9b80e773bfc1906be12ac9f31fe -->

<!-- START_5270b231c7de3f74bad637da911d8447 -->
## users/search

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//users/search" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//users/search",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST users/search`


<!-- END_5270b231c7de3f74bad637da911d8447 -->

<!-- START_ba44b4346c1a9d245950989d5be744fe -->
## user/generate

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//user/generate" \
-H "Accept: application/json" \
    -d "name"="id" \
    -d "surname"="id" \
    -d "passssword"="id" \
    -d "email"="moshe20@example.com" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//user/generate",
    "method": "POST",
    "data": {
        "name": "id",
        "surname": "id",
        "passssword": "id",
        "email": "moshe20@example.com"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST user/generate`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | Maximum: `255`
    surname | string |  optional  | Maximum: `255`
    passssword | string |  optional  | Maximum: `255`
    email | email |  optional  | Maximum: `255`

<!-- END_ba44b4346c1a9d245950989d5be744fe -->

<!-- START_6efa42ffd4a3539d67302796569ea124 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//buildings" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//buildings",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "buildings": [
        {
            "id": 3,
            "user_id": null,
            "image_id": 6,
            "zipCode": "37011",
            "state": "Italia",
            "country": "VR",
            "city": "Bardolino",
            "address": "strada Spinarol",
            "latitude": "0.000",
            "longitude": "0.000",
            "created_at": "2018-05-22 14:25:24",
            "updated_at": "2018-05-22 14:28:17",
            "deleted_at": null,
            "image": {
                "id": 6,
                "token": "http:\/\/hellocreativestudio.net\/myFlat\/serverU0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y",
                "50w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/U0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y_100x100.jpg",
                "100w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/U0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y_100x100.jpg",
                "300w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/U0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y_300x300.jpg",
                "1000w": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/thumb\/U0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y_1000x1000.jpg",
                "original": "http:\/\/hellocreativestudio.net\/myFlat\/server\/images\/original\/U0jLStPsQsl3kxXF2ZdEmgz5RQriYpDN82U8fi5Y.jpg"
            },
            "user": null
        }
    ]
}
```

### HTTP Request
`GET buildings`

`HEAD buildings`


<!-- END_6efa42ffd4a3539d67302796569ea124 -->

<!-- START_fd1ad9070623881bae028d002fdf16e6 -->
## building/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//building/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "building": null
}
```

### HTTP Request
`GET building/{id}`

`HEAD building/{id}`


<!-- END_fd1ad9070623881bae028d002fdf16e6 -->

<!-- START_2318bb9b5936c2d19d24a7e7e384169d -->
## building/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//building/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/create",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST building/create`


<!-- END_2318bb9b5936c2d19d24a7e7e384169d -->

<!-- START_146f74001f7a8381b9054715f3556596 -->
## building/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//building/{id}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/{id}/edit",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST building/{id}/edit`


<!-- END_146f74001f7a8381b9054715f3556596 -->

<!-- START_c160a8f970f8c725fca9ad9437dc04ac -->
## building/{id}/image/add

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//building/{id}/image/add" \
-H "Accept: application/json" \
    -d "image"="aut" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/{id}/image/add",
    "method": "POST",
    "data": {
        "image": "aut"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST building/{id}/image/add`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    image | image |  required  | Must be an image (jpeg, png, bmp, gif, or svg) Maximum: `10240  `

<!-- END_c160a8f970f8c725fca9ad9437dc04ac -->

<!-- START_983ba1265eba5cc36562cd38ba91afa1 -->
## building/{id}/image/{image_id}/remove

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//building/{id}/image/{image_id}/remove" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/{id}/image/{image_id}/remove",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET building/{id}/image/{image_id}/remove`

`HEAD building/{id}/image/{image_id}/remove`


<!-- END_983ba1265eba5cc36562cd38ba91afa1 -->

<!-- START_b5c751956accfbbfc4c37157ffdb43c1 -->
## building/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//building/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//building/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET building/{id}/delete`

`HEAD building/{id}/delete`


<!-- END_b5c751956accfbbfc4c37157ffdb43c1 -->

<!-- START_0832ee190696bee3c256e98299ea85f9 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//flats" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flats",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "flats": [
        {
            "id": 2,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "13A",
            "scale": "2C",
            "created_at": "2018-05-08 11:47:37",
            "updated_at": "2018-05-08 11:47:37",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 3,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "2",
            "scale": "8A",
            "created_at": "2018-05-22 11:02:46",
            "updated_at": "2018-05-22 11:02:46",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 4,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "2",
            "scale": "7C",
            "created_at": "2018-05-22 11:05:09",
            "updated_at": "2018-05-22 11:05:09",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 5,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "5",
            "scale": "7c",
            "created_at": "2018-05-22 11:06:04",
            "updated_at": "2018-05-22 11:06:04",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 6,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "w",
            "scale": "w",
            "created_at": "2018-05-22 11:06:25",
            "updated_at": "2018-05-22 11:06:25",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 8,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "s",
            "scale": "s",
            "created_at": "2018-05-22 11:07:51",
            "updated_at": "2018-05-22 11:07:51",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 11,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "sa",
            "scale": "sa",
            "created_at": "2018-05-22 11:10:32",
            "updated_at": "2018-05-22 11:10:32",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        },
        {
            "id": 12,
            "building_id": 1,
            "image_id": null,
            "user_id": 3,
            "internal": "7",
            "scale": "8c",
            "created_at": "2018-05-22 11:14:02",
            "updated_at": "2018-05-22 11:14:02",
            "deleted_at": null,
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            },
            "image": null
        }
    ]
}
```

### HTTP Request
`GET flats`

`HEAD flats`


<!-- END_0832ee190696bee3c256e98299ea85f9 -->

<!-- START_b2c25cb995ec5c858d2c72d19d8feb39 -->
## flat/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//flat/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "flat": null
}
```

### HTTP Request
`GET flat/{id}`

`HEAD flat/{id}`


<!-- END_b2c25cb995ec5c858d2c72d19d8feb39 -->

<!-- START_767fe85b464deb56bb3fd5d10a9a3453 -->
## flat/user/{user_id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//flat/user/{user_id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/user/{user_id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "flat": []
}
```

### HTTP Request
`GET flat/user/{user_id}`

`HEAD flat/user/{user_id}`


<!-- END_767fe85b464deb56bb3fd5d10a9a3453 -->

<!-- START_2a0d1e55cc6b98993ec695bbb75df28c -->
## flat/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//flat/create" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/create",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST flat/create`


<!-- END_2a0d1e55cc6b98993ec695bbb75df28c -->

<!-- START_96d625c8d37df9bb75da832051783f32 -->
## flat/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//flat/{id}/edit" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/{id}/edit",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST flat/{id}/edit`


<!-- END_96d625c8d37df9bb75da832051783f32 -->

<!-- START_3c27525a65caa24c90fbdff569c34a34 -->
## flat/{id}/image/add

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//flat/{id}/image/add" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/{id}/image/add",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST flat/{id}/image/add`


<!-- END_3c27525a65caa24c90fbdff569c34a34 -->

<!-- START_f499d46dba1c25a4bcf89228ca6d4490 -->
## flat/{id}/image/{image_id}/remove

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//flat/{id}/image/{image_id}/remove" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/{id}/image/{image_id}/remove",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET flat/{id}/image/{image_id}/remove`

`HEAD flat/{id}/image/{image_id}/remove`


<!-- END_f499d46dba1c25a4bcf89228ca6d4490 -->

<!-- START_b7c236d3195fe9a1c89a7db7a2d1a91c -->
## flat/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//flat/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//flat/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET flat/{id}/delete`

`HEAD flat/{id}/delete`


<!-- END_b7c236d3195fe9a1c89a7db7a2d1a91c -->

<!-- START_24839da1443f6efc4c99cbc024e4b3bf -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claims" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claims",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "claims": [
        {
            "id": 2,
            "category_id": 2,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "flat": {
                "id": 3,
                "building_id": 1,
                "image_id": null,
                "user_id": 3,
                "internal": "2",
                "scale": "8A",
                "created_at": "2018-05-22 11:02:46",
                "updated_at": "2018-05-22 11:02:46",
                "deleted_at": null
            },
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            }
        },
        {
            "id": 3,
            "category_id": 3,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "flat": {
                "id": 3,
                "building_id": 1,
                "image_id": null,
                "user_id": 3,
                "internal": "2",
                "scale": "8A",
                "created_at": "2018-05-22 11:02:46",
                "updated_at": "2018-05-22 11:02:46",
                "deleted_at": null
            },
            "user": {
                "id": 3,
                "name": "Matteo",
                "surname": "Barato",
                "type": "0",
                "admin": 1,
                "email": "matteo@hcslab.it",
                "deleted_at": null,
                "created_at": "2018-05-14 13:53:58",
                "updated_at": "2018-05-14 13:53:58"
            }
        }
    ]
}
```

### HTTP Request
`GET claims`

`HEAD claims`


<!-- END_24839da1443f6efc4c99cbc024e4b3bf -->

<!-- START_09c952d1c024fbeff8c36fd320ce1157 -->
## claims/relations

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claims/relations" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claims/relations",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "claims": [
        {
            "id": 2,
            "category_id": 2,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "category": {
                "id": 2,
                "name": "Guasto Idraulico",
                "created_at": null,
                "updated_at": "2018-05-24 09:02:35",
                "deleted_at": null
            },
            "room": {
                "id": 2,
                "name": "Sala \/ soggiorno",
                "created_at": null,
                "updated_at": "2018-05-24 10:13:49",
                "deleted_at": null
            },
            "problem": {
                "id": 2,
                "name": "Forno",
                "category_id": 3,
                "room_id": 1,
                "created_at": null,
                "updated_at": "2018-05-24 10:21:12",
                "deleted_at": null
            },
            "status": {
                "id": 0,
                "name": "In attesa visualizzazione",
                "created_at": null,
                "updated_at": "2018-05-24 11:39:47",
                "deleted_at": null
            }
        },
        {
            "id": 3,
            "category_id": 3,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "category": {
                "id": 3,
                "name": "Guasto Generico",
                "created_at": null,
                "updated_at": "2018-05-24 09:02:41",
                "deleted_at": null
            },
            "room": {
                "id": 3,
                "name": "Bagno",
                "created_at": null,
                "updated_at": "2018-05-24 10:15:01",
                "deleted_at": null
            },
            "problem": {
                "id": 3,
                "name": "Frigorifero \/ congelatore",
                "category_id": 3,
                "room_id": 1,
                "created_at": null,
                "updated_at": "2018-05-24 10:21:12",
                "deleted_at": null
            },
            "status": {
                "id": 0,
                "name": "In attesa visualizzazione",
                "created_at": null,
                "updated_at": "2018-05-24 11:39:47",
                "deleted_at": null
            }
        }
    ]
}
```

### HTTP Request
`GET claims/relations`

`HEAD claims/relations`


<!-- END_09c952d1c024fbeff8c36fd320ce1157 -->

<!-- START_c9c50bb88e963cc0a83049d8d25b224e -->
## claims/user/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claims/user/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claims/user/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "claims": [
        {
            "id": 2,
            "category_id": 2,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "category": {
                "id": 2,
                "name": "Guasto Idraulico",
                "created_at": null,
                "updated_at": "2018-05-24 09:02:35",
                "deleted_at": null
            },
            "room": {
                "id": 2,
                "name": "Sala \/ soggiorno",
                "created_at": null,
                "updated_at": "2018-05-24 10:13:49",
                "deleted_at": null
            },
            "problem": {
                "id": 2,
                "name": "Forno",
                "category_id": 3,
                "room_id": 1,
                "created_at": null,
                "updated_at": "2018-05-24 10:21:12",
                "deleted_at": null
            },
            "flat": {
                "id": 3,
                "building_id": 1,
                "image_id": null,
                "user_id": 3,
                "internal": "2",
                "scale": "8A",
                "created_at": "2018-05-22 11:02:46",
                "updated_at": "2018-05-22 11:02:46",
                "deleted_at": null,
                "building": null
            },
            "status": {
                "id": 0,
                "name": "In attesa visualizzazione",
                "created_at": null,
                "updated_at": "2018-05-24 11:39:47",
                "deleted_at": null
            }
        },
        {
            "id": 3,
            "category_id": 3,
            "room_id": 2,
            "problem_id": 2,
            "user_id": 3,
            "flat_id": 3,
            "note": null,
            "status_id": 0,
            "created_at": "2018-05-21 09:06:00",
            "updated_at": null,
            "deleted_at": null,
            "category": {
                "id": 3,
                "name": "Guasto Generico",
                "created_at": null,
                "updated_at": "2018-05-24 09:02:41",
                "deleted_at": null
            },
            "room": {
                "id": 3,
                "name": "Bagno",
                "created_at": null,
                "updated_at": "2018-05-24 10:15:01",
                "deleted_at": null
            },
            "problem": {
                "id": 3,
                "name": "Frigorifero \/ congelatore",
                "category_id": 3,
                "room_id": 1,
                "created_at": null,
                "updated_at": "2018-05-24 10:21:12",
                "deleted_at": null
            },
            "flat": {
                "id": 3,
                "building_id": 1,
                "image_id": null,
                "user_id": 3,
                "internal": "2",
                "scale": "8A",
                "created_at": "2018-05-22 11:02:46",
                "updated_at": "2018-05-22 11:02:46",
                "deleted_at": null,
                "building": null
            },
            "status": {
                "id": 0,
                "name": "In attesa visualizzazione",
                "created_at": null,
                "updated_at": "2018-05-24 11:39:47",
                "deleted_at": null
            }
        }
    ]
}
```

### HTTP Request
`GET claims/user/{id}`

`HEAD claims/user/{id}`


<!-- END_c9c50bb88e963cc0a83049d8d25b224e -->

<!-- START_3e7a457c62e70be3a957300f691a609d -->
## claim/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claim/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "claim": null
}
```

### HTTP Request
`GET claim/{id}`

`HEAD claim/{id}`


<!-- END_3e7a457c62e70be3a957300f691a609d -->

<!-- START_87f6bf0ac06e68dc90edaec39b5db4e2 -->
## claim/rel/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claim/rel/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/rel/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "claim": null
}
```

### HTTP Request
`GET claim/rel/{id}`

`HEAD claim/rel/{id}`


<!-- END_87f6bf0ac06e68dc90edaec39b5db4e2 -->

<!-- START_40494112bbccc2697ffac74d27e5b228 -->
## claim/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//claim/create" \
-H "Accept: application/json" \
    -d "id"="383107" \
    -d "user_id"="383107" \
    -d "flat_id"="383107" \
    -d "category_id"="383107" \
    -d "problem_id"="383107" \
    -d "room_id"="383107" \
    -d "dates"="facilis" \
    -d "note"="facilis" \
    -d "completed"="1" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/create",
    "method": "POST",
    "data": {
        "id": 383107,
        "user_id": 383107,
        "flat_id": 383107,
        "category_id": 383107,
        "problem_id": 383107,
        "room_id": 383107,
        "dates": "facilis",
        "note": "facilis",
        "completed": true
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST claim/create`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    user_id | integer |  required  | Valid user id
    flat_id | integer |  required  | Valid flat id
    category_id | integer |  required  | Valid claims_category id
    problem_id | integer |  required  | Valid claims_category id
    room_id | integer |  required  | Valid claims_category id
    dates | array |  required  | 
    note | string |  optional  | 
    completed | boolean |  optional  | 

<!-- END_40494112bbccc2697ffac74d27e5b228 -->

<!-- START_b99a1e652f35b7585af89c7d8c958959 -->
## claim/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//claim/{id}/edit" \
-H "Accept: application/json" \
    -d "id"="615166" \
    -d "user_id"="615166" \
    -d "flat_id"="615166" \
    -d "category_id"="615166" \
    -d "problem_id"="615166" \
    -d "room_id"="615166" \
    -d "dates"="facilis" \
    -d "note"="facilis" \
    -d "completed"="1" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}/edit",
    "method": "POST",
    "data": {
        "id": 615166,
        "user_id": 615166,
        "flat_id": 615166,
        "category_id": 615166,
        "problem_id": 615166,
        "room_id": 615166,
        "dates": "facilis",
        "note": "facilis",
        "completed": true
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST claim/{id}/edit`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    user_id | integer |  required  | Valid user id
    flat_id | integer |  required  | Valid flat id
    category_id | integer |  required  | Valid claims_category id
    problem_id | integer |  required  | Valid claims_category id
    room_id | integer |  required  | Valid claims_category id
    dates | array |  required  | 
    note | string |  optional  | 
    completed | boolean |  optional  | 

<!-- END_b99a1e652f35b7585af89c7d8c958959 -->

<!-- START_bae445466291fdd9b6fac6aac4367488 -->
## claim/{id}/completed

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//claim/{id}/completed" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}/completed",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST claim/{id}/completed`


<!-- END_bae445466291fdd9b6fac6aac4367488 -->

<!-- START_bc98844f3c5e1202e27102dac4ed55ce -->
## claim/{id}/image/add

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//claim/{id}/image/add" \
-H "Accept: application/json" \
    -d "image"="expedita" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}/image/add",
    "method": "POST",
    "data": {
        "image": "expedita"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST claim/{id}/image/add`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    image | image |  required  | Must be an image (jpeg, png, bmp, gif, or svg) Maximum: `10240  `

<!-- END_bc98844f3c5e1202e27102dac4ed55ce -->

<!-- START_5d9a7ead5b136b745152e9953121ed47 -->
## claim/{id}/image/{image_id}/remove

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claim/{id}/image/{image_id}/remove" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}/image/{image_id}/remove",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET claim/{id}/image/{image_id}/remove`

`HEAD claim/{id}/image/{image_id}/remove`


<!-- END_5d9a7ead5b136b745152e9953121ed47 -->

<!-- START_9a504889dfe6a091e64adb20ba3697ba -->
## claim/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//claim/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//claim/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET claim/{id}/delete`

`HEAD claim/{id}/delete`


<!-- END_9a504889dfe6a091e64adb20ba3697ba -->

<!-- START_2f1038cabc13b0dbcd0e043a404758b2 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//categories" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//categories",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "category": [
        {
            "id": 2,
            "name": "Guasto Idraulico",
            "created_at": null,
            "updated_at": "2018-05-24 09:02:35",
            "deleted_at": null
        },
        {
            "id": 3,
            "name": "Guasto Generico",
            "created_at": null,
            "updated_at": "2018-05-24 09:02:41",
            "deleted_at": null
        },
        {
            "id": 4,
            "name": "Segnalazione generica",
            "created_at": null,
            "updated_at": "2018-05-24 09:02:52",
            "deleted_at": null
        }
    ]
}
```

### HTTP Request
`GET categories`

`HEAD categories`


<!-- END_2f1038cabc13b0dbcd0e043a404758b2 -->

<!-- START_89ef5cabc1b078c1e3eaec8c37006634 -->
## category/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//category/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//category/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "category": null
}
```

### HTTP Request
`GET category/{id}`

`HEAD category/{id}`


<!-- END_89ef5cabc1b078c1e3eaec8c37006634 -->

<!-- START_6bcf3c70d8fbd4f4589779fe184460e6 -->
## category/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//category/create" \
-H "Accept: application/json" \
    -d "id"="22" \
    -d "name"="consequuntur" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//category/create",
    "method": "POST",
    "data": {
        "id": 22,
        "name": "consequuntur"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST category/create`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 

<!-- END_6bcf3c70d8fbd4f4589779fe184460e6 -->

<!-- START_7b6bdbf1f61677b19f7d58e1a657e34e -->
## category/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//category/{id}/edit" \
-H "Accept: application/json" \
    -d "id"="902469722" \
    -d "name"="ut" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//category/{id}/edit",
    "method": "POST",
    "data": {
        "id": 902469722,
        "name": "ut"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST category/{id}/edit`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 

<!-- END_7b6bdbf1f61677b19f7d58e1a657e34e -->

<!-- START_97d8e840c69f035a855d0a88ce4e22c7 -->
## category/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//category/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//category/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET category/{id}/delete`

`HEAD category/{id}/delete`


<!-- END_97d8e840c69f035a855d0a88ce4e22c7 -->

<!-- START_3eda80a70e019abcbcf64681ebfc3398 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//rooms" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//rooms",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "room": [
        {
            "id": 2,
            "name": "Sala \/ soggiorno",
            "created_at": null,
            "updated_at": "2018-05-24 10:13:49",
            "deleted_at": null
        },
        {
            "id": 3,
            "name": "Bagno",
            "created_at": null,
            "updated_at": "2018-05-24 10:15:01",
            "deleted_at": null
        },
        {
            "id": 4,
            "name": "Cantina",
            "created_at": null,
            "updated_at": "2018-05-24 10:15:17",
            "deleted_at": null
        }
    ]
}
```

### HTTP Request
`GET rooms`

`HEAD rooms`


<!-- END_3eda80a70e019abcbcf64681ebfc3398 -->

<!-- START_3d426f84a1d66373f64bccba850211b1 -->
## room/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//room/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//room/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "room": null
}
```

### HTTP Request
`GET room/{id}`

`HEAD room/{id}`


<!-- END_3d426f84a1d66373f64bccba850211b1 -->

<!-- START_667a46ed51adb053f80beeb482e15e56 -->
## room/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//room/create" \
-H "Accept: application/json" \
    -d "id"="4" \
    -d "name"="perferendis" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//room/create",
    "method": "POST",
    "data": {
        "id": 4,
        "name": "perferendis"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST room/create`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 

<!-- END_667a46ed51adb053f80beeb482e15e56 -->

<!-- START_778720adfcb6f50ef7847432ea809602 -->
## room/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//room/{id}/edit" \
-H "Accept: application/json" \
    -d "id"="916311920" \
    -d "name"="sapiente" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//room/{id}/edit",
    "method": "POST",
    "data": {
        "id": 916311920,
        "name": "sapiente"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST room/{id}/edit`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 

<!-- END_778720adfcb6f50ef7847432ea809602 -->

<!-- START_fe6fd7df7e9303ac0b4059f60a634584 -->
## room/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//room/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//room/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET room/{id}/delete`

`HEAD room/{id}/delete`


<!-- END_fe6fd7df7e9303ac0b4059f60a634584 -->

<!-- START_a3cdacd5a4fdfc1bfc4dc7d8aa1c3a26 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//problems" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//problems",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "problem": [
        {
            "id": 2,
            "name": "Forno",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 3,
            "name": "Frigorifero \/ congelatore",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 4,
            "name": "Piano cottura",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 5,
            "name": "Rubinetto",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 6,
            "name": "Piastre",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 7,
            "name": "Mobilio della cucina",
            "category_id": 3,
            "room_id": 1,
            "created_at": null,
            "updated_at": "2018-05-24 10:21:12",
            "deleted_at": null
        },
        {
            "id": 8,
            "name": "Toilette",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 9,
            "name": "Doccia",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 10,
            "name": "Vasca",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 11,
            "name": "Rubinetto",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 12,
            "name": "Bidè",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 13,
            "name": "Specchio",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 14,
            "name": "Asciugatrice",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 15,
            "name": "Lavatrice",
            "category_id": 3,
            "room_id": 3,
            "created_at": null,
            "updated_at": "2018-05-24 10:22:26",
            "deleted_at": null
        },
        {
            "id": 16,
            "name": "Cantina vini",
            "category_id": 3,
            "room_id": 4,
            "created_at": null,
            "updated_at": "2018-05-24 10:23:02",
            "deleted_at": null
        },
        {
            "id": 17,
            "name": "Temperatura & Umidità",
            "category_id": 3,
            "room_id": 4,
            "created_at": null,
            "updated_at": "2018-05-24 10:23:02",
            "deleted_at": null
        },
        {
            "id": 18,
            "name": "Odore",
            "category_id": 3,
            "room_id": 4,
            "created_at": null,
            "updated_at": "2018-05-24 10:23:02",
            "deleted_at": null
        }
    ]
}
```

### HTTP Request
`GET problems`

`HEAD problems`


<!-- END_a3cdacd5a4fdfc1bfc4dc7d8aa1c3a26 -->

<!-- START_b8c6f032f25500e52410b85014417ec8 -->
## problem/{id}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//problem/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//problem/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "problem": null
}
```

### HTTP Request
`GET problem/{id}`

`HEAD problem/{id}`


<!-- END_b8c6f032f25500e52410b85014417ec8 -->

<!-- START_86952e8620e3af884119ab513561fe30 -->
## problem/create

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//problem/create" \
-H "Accept: application/json" \
    -d "id"="2" \
    -d "name"="ipsa" \
    -d "category_id"="2" \
    -d "room_id"="2" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//problem/create",
    "method": "POST",
    "data": {
        "id": 2,
        "name": "ipsa",
        "category_id": 2,
        "room_id": 2
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST problem/create`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 
    category_id | integer |  required  | 
    room_id | integer |  required  | 

<!-- END_86952e8620e3af884119ab513561fe30 -->

<!-- START_4f829ad3565d5ddd951f9ff705a8fd61 -->
## problem/{id}/edit

> Example request:

```bash
curl -X POST "http://hellocreativestudio.net/myFlat/server//problem/{id}/edit" \
-H "Accept: application/json" \
    -d "id"="2228" \
    -d "name"="aliquid" \
    -d "category_id"="2228" \
    -d "room_id"="2228" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//problem/{id}/edit",
    "method": "POST",
    "data": {
        "id": 2228,
        "name": "aliquid",
        "category_id": 2228,
        "room_id": 2228
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST problem/{id}/edit`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id | integer |  optional  | 
    name | string |  required  | 
    category_id | integer |  required  | 
    room_id | integer |  required  | 

<!-- END_4f829ad3565d5ddd951f9ff705a8fd61 -->

<!-- START_9f214b8087887c95662b19cbf9238cdf -->
## problem/{id}/delete

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//problem/{id}/delete" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//problem/{id}/delete",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET problem/{id}/delete`

`HEAD problem/{id}/delete`


<!-- END_9f214b8087887c95662b19cbf9238cdf -->

<!-- START_cfd6adcd4775cd0de33c05f398a73d21 -->
## images/thumb/{path}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//images/thumb/{path}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//images/thumb/{path}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET images/thumb/{path}`

`HEAD images/thumb/{path}`


<!-- END_cfd6adcd4775cd0de33c05f398a73d21 -->

<!-- START_3447030729dc421c0336aeeea0c340a0 -->
## images/scale/{path}

> Example request:

```bash
curl -X GET "http://hellocreativestudio.net/myFlat/server//images/scale/{path}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://hellocreativestudio.net/myFlat/server//images/scale/{path}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET images/scale/{path}`

`HEAD images/scale/{path}`


<!-- END_3447030729dc421c0336aeeea0c340a0 -->

