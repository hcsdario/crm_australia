<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->string('file_title')->nullable();
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('Customers');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('Users');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            //$table->dropForeign('tasks_priority_id_foreign');
            $table->dropForeign('files_user_id_foreign');
            $table->dropForeign('files_customer_id_foreign');
        });

        Schema::dropIfExists('Files');
    }
}
