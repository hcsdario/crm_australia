<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPassportToInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profilo', function (Blueprint $table) {
        $table->integer('passaporto')->after('hobby2');
        $table->integer('scadenza_passaporto')->after('passaporto');
        $table->renameColumn('scadenza', 'cartaidentita');
        $table->string('scadenza', 50)->change();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profilo', function (Blueprint $table) {
            $table->dropColumn('passaporto');
            $table->dropColumn('scadenza_passaporto');
            $table->renameColumn('cartaidentita', 'scadenza');
            $table->date('cartaidentita')->change();
        });
    }
}
