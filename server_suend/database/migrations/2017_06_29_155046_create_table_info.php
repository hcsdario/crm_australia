<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('indirizzo')->nullable();
            $table->string('sito_web')->nullable();
            $table->string('partita_iva')->nullable();
            $table->string('codice_fiscale')->nullable();
            $table->date('data_nascita')->nullable();
            $table->string('sesso')->nullable();
            $table->string('posizione')->nullable();
            $table->string('suffisso')->nullable();
            $table->string('tel4')->nullable();
            $table->string('tel5')->nullable();
            $table->string('fax1')->nullable();
            $table->string('fax2')->nullable();
            $table->string('email2')->nullable();
            $table->string('indirizzo2')->nullable();
            $table->string('citta2')->nullable();
            $table->string('provincia2')->nullable();
            $table->string('cap2')->nullable();
            $table->string('regione2')->nullable();
            $table->string('stato2')->nullable();
            $table->boolean('privacy')->default(0);
            $table->boolean('privacy_commerciali')->default(0);
            $table->boolean('privacy_profilazione')->default(0);

            //foreign key per l'user
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('Customers');
            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info', function (Blueprint $table) {
            $table->dropForeign('info_customer_id_foreign');
        });

        Schema::dropIfExists('info');
    }
}
