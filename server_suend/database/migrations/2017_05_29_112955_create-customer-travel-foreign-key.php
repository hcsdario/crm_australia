<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTravelForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('travels', function (Blueprint $table) {
		    $table->unsignedInteger('customer_id');
		    $table->foreign('customer_id')->references('id')->on('Customers');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('travels', function (Blueprint $table) {
		    $table->dropForeign('travels_customer_id_foreign');
	    });
    }
}
