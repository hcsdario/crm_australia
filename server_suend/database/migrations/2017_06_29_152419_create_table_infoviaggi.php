<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfoviaggi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infoviaggi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacanze_anno')->nullable();
            $table->string('preferenza_viaggi')->nullable();
            $table->string('durata_media')->nullable();
            $table->string('prossimi_paesi')->nullable();
            $table->string('fonti')->nullable();
            $table->string('note_tematiche')->nullable();

            $table->boolean('gennaio')->default(false);
            $table->boolean('febbraio')->default(false);
            $table->boolean('marzo')->default(false);
            $table->boolean('aprile')->default(false);
            $table->boolean('maggio')->default(false);
            $table->boolean('giugno')->default(false);
            $table->boolean('luglio')->default(false);
            $table->boolean('agosto')->default(false);
            $table->boolean('settembre')->default(false);
            $table->boolean('ottobre')->default(false);
            $table->boolean('novembre')->default(false);
            $table->boolean('dicembre')->default(false);

            $table->boolean('archeologia')->default(false);
            $table->boolean('avventura')->default(false);
            $table->boolean('cultura')->default(false);
            $table->boolean('ecoturismo')->default(false);
            $table->boolean('enogastronomia')->default(false);
            $table->boolean('fotografico')->default(false);
            $table->boolean('mare_relax')->default(false);
            $table->boolean('montagna')->default(false);
            $table->boolean('religioso')->default(false);
            $table->boolean('sportivo')->default(false);
            $table->boolean('storico_politico')->default(false);
            $table->boolean('altro')->default(false);

            $table->string('destinazioni_preferite')->nullable();
            $table->string('destinazioni_passate')->nullable();

            //foreign key per il customer
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('Customers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('infoviaggi', function (Blueprint $table) {
            $table->dropForeign('infoviaggi_customer_id_foreign');
        });

        Schema::dropIfExists('infoviaggi');
    }
}
