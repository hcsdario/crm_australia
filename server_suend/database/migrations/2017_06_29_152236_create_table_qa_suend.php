<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQaSuend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_suend', function (Blueprint $table) {
            $table->increments('id');
            $table->string('conoscenza_suend')->nullable();
            $table->string('note_conoscenza_suend')->nullable();
            $table->string('codice_cliente')->nullable();
            $table->boolean('gia_viaggiato')->default(false);
            $table->integer('quante_volte')->nullable();
            $table->string('luogo_viaggio')->nullable();

            $table->boolean('conosce_sito')->deault(false);
            $table->string('note_qa_suend')->nullable();

            //foreign key per il customer
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('Customers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_suend', function (Blueprint $table) {
            $table->dropForeign('qa_suend_customer_id_foreign');
        });

        Schema::dropIfExists('qa_suend');
    }
}
