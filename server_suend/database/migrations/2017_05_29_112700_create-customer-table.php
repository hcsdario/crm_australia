<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		    Schema::create('customers', function (Blueprint $table) {
			    $table->increments('id');
			    $table->string('codice')->nullable();
			    $table->string('societa')->nullable();
			    $table->string('cognome')->nullable();
			    $table->string('nome')->nullable();
			    $table->date('data_nascita')->nullable();
			    $table->string('citta')->nullable();
			    $table->string('provincia')->nullable();
			    $table->string('cap')->nullable();
			    $table->string('regione')->nullable();
			    $table->string('stato')->nullable();
			    $table->string('indirizzo')->nullable();
			    $table->string('sito_web')->nullable();
			    $table->string('partita_iva')->nullable();
			    $table->string('codice_fiscale')->nullable();
			    $table->string('sesso')->nullable();
			    $table->string('posizione')->nullable();
			    $table->string('suffisso')->nullable();
			    $table->string('tel1')->nullable();
			    $table->string('tel2')->nullable();
			    $table->string('tel3')->nullable();
			    $table->string('tel4')->nullable();
			    $table->string('tel5')->nullable();
			    $table->string('fax1')->nullable();
			    $table->string('fax2')->nullable();
			    $table->string('email1')->nullable();
			    $table->string('email2')->nullable();
			    $table->string('indirizzo2')->nullable();
			    $table->string('citta2')->nullable();
			    $table->string('provincia2')->nullable();
			    $table->string('cap2')->nullable();
			    $table->boolean('privacy')->default(0);
			    $table->boolean('privacy_commerciali')->default(0);
			    $table->boolean('privacy_profilazione')->default(0);
			    $table->timestamps();
			    $table->softDeletes();
		    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		    Schema::dropIfExists('customers');
    }
}
