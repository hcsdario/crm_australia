<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfilo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilo', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_nascita')->nullable();
            $table->string('luogonascita')->nullable();
            $table->string('professione')->nullable();
            $table->string('hobby1')->nullable();
            $table->string('hobby2')->nullable();
            $table->date('scadenza')->nullable();
            $table->date('scadenza_cartaidentita')->nullable();

            $table->longText('frequent_flyer')->nullable();
            $table->boolean('newsletter')->dafault(false);
            $table->boolean('cliente_diretto')->nullable();
            $table->integer('adulti')->default(0);
            $table->integer('bambini')->default(0);
            $table->integer('neonati')->default(0);
            $table->integer('figli')->default(0);
            $table->integer('anziani')->default(0);
            $table->integer('animali')->default(0);
            $table->boolean('iscritto_club')->default(false);
            $table->string('codice_club')->nullable();
            $table->longText('note')->nullable();

            //foreign key per il customer
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profilo', function (Blueprint $table) {
            $table->dropForeign('profilo_customer_id_foreign');
        });

        Schema::dropIfExists('profilo');
    }
}
