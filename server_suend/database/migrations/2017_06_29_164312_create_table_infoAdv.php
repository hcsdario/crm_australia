<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInfoAdv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_adv', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fatturato_medio')->nullable();
            $table->string('numero_addetti')->nullable();
            $table->string('clienti')->nullable();
            $table->string('destinazioni_vendute1')->nullable();
            $table->string('destinazioni_vendute2')->nullable();
            $table->string('destinazioni_vendute3')->nullable();
            $table->string('destinazioni_vendute4')->nullable();
            $table->string('destinazioni_vendute5')->nullable();
            $table->string('destinazioni_vendute6')->nullable();
            $table->string('destinazioni_vendute7')->nullable();
            $table->string('destinazioni_vendute8')->nullable();

            $table->longText('business_travel')->nullable();
            $table->boolean('leisure')->dafault(false);
            $table->string('biglietteria')->nullable();

            //foreign key per l'user
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('Customers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_adv', function (Blueprint $table) {
            $table->dropForeign('info_adv_customer_id_foreign');
        });

        Schema::dropIfExists('info_adv');
    }
}
