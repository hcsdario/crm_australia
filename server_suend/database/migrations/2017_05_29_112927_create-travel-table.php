<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		    Schema::create('Travels', function (Blueprint $table) {
			    $table->increments( 'id' );
			    $table->string('soddisfazione');
			    $table->date( 'data_partenza' );
			    $table->date( 'ultima_rilevazione' );
			    $table->string( 'destinazione' );
			    $table->longText( 'storico' );
			    $table->timestamps();
			    $table->softDeletes();
		    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		    Schema::dropIfExists('Travels');
    }
}
