<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Facades\Datatables;

class FilesController extends Controller
{
    public function data(Datatables $datatables)
    {
        $query = File::select('id','file_name','created_at');

        return Datatables::of($query)
            ->addColumn('azioni', function($query) {
                if (Auth::user()->admin == 1) {
                    $url = '../admin/cliente/' . $query->id;
                } else {
                    $url = '../user/cliente/' . $query->id;
                }
                $path = '../../crm/storage/app/' .$query->file_name;
                $delete_path = '../file/delete/'. $query->id;
                return '<a href="' . $path . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>&nbsp;Visualizza File</a>&nbsp;&nbsp;<a href="'. $delete_path.'" class="btn btn-xs btn-warning"><i class="fa fa-eraser"></i>&nbsp;Elimina File</a>';
            })->rawColumns(['azioni'])->make(true);
    }
}
