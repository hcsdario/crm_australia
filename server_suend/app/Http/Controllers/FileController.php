<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Requests\UploadPdfRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     //   dd('entra');
        try {
            if ($request) {

                $path = $request->file('file_upload')->store('suend');

                $file = new File();
                $file->file_name = $path;
                $file->customer_id = $request->customer_id;
                $file->user_id = Auth::id();
                $file->save();
                return redirect()->back()->with('success', 'Il File e\' stato inserito!');
            }

        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento File!' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logicDelete($id)
    {
        try {
            $files = File::find($id);
            if($files) {
                $files->delete();
                return redirect()->back()->with('success', 'File eliminato!');
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Errore Inserimento File!' . $e);
        }

    }
}
