<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Uploads;
use App\Task;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class uploadsController extends Controller
{
    public $comuniCtrl;

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin/uploads',[  'task_total' => $task_total,'task_open' => $task_total] );
    }

    public function index_settings()
    {
     
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin/settings',['task_total' => $task_total,'task_open' => $task_total] );
    }

    public function update_settings(Request $request)
    {
     
        $this->log("settings","update_settings",$request->all());
        file_put_contents(getcwd()."/crm/resources/man.html", $request['wysiwg_full']);
        $user_id = Auth::user()->id;
        $task_total =  Task::where(['user_id'=>$user_id,'status' => 'attivo'])->orWhere(['user_assigned_id'=>$user_id,'status' => 'attivo'])->orderBy('category_id','asc')->orderBy('task_deadline','asc')->get();
        $task_total = count($task_total);

        return view('admin/settings',['task_total' => $task_total,'task_open' => $task_total] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
        {

            $filename=$request->file('files')->getClientOriginalName();
            $zone=$request->get('zone');
            if($zone=="Tutte le zone")
              $zone="Tutte";    

            $uploads = new Uploads(array(
              'files' => $filename,
              'label' => $filename,
              'userid'  => $request->get('userid'),
              'dir'  => $zone
            ));

            $uploads->save();
           

            $fileName = $filename;
            $path=base_path() . '/upload/'. $zone . "/" ;
            $request->file('files')->move( $path , $fileName );

            return \Redirect::route('uploads')->with('message', 'File caricati!');    
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Datatables $datatables)
    {
    
    }
}
