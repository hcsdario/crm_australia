<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{

	public function __construct(TaskController $tasks)
	{
        $this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $user_id = Auth::user()->id;
        $today=date("Y-m-d");
        session_start();
        $_SESSION['userid']=$user_id;

        $task_total = 18;
        $task_done = 12;
        $myPendingTask = $task_total-$task_done;

        return view('admin.dashboard',
        [
            'tasks' => [], 
            'users' => [], 
            'categories' => [], 
            'task_total' => 0,
            'news' => [],
            'task_done' => 0,
            'myPendingTask' => 0,
            'task_open' => 0
        ]

        );

    }


}
