<?php

namespace App\Http\Controllers;

use App\CityModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComuniController extends Controller
{
    public function getRegione() {
        $regioni = DB::table('italy_regions')->get();
        return $regioni;
    }

    public function getProvincieByIdRegione($regione_id) {

        $provincie = DB::table('italy_provincies')->where('id_regione', $regione_id)->get();
        return $provincie;
    }

    public function getCittaBySiglaProvincia($sigla) {
        $citta = DB::table('italy_cities')->where('provincia', $sigla)->get();
        return $citta;
    }


    public function autocomplete(Request $request) {

        $data = DB::table('italy_cities')
            ->leftJoin("italy_provincies","italy_provincies.provincia","italy_provincies.sigla")
            ->where("italy_cities.comune","LIKE","{$request->input('term')}%")
            ->where("italy_cities.estera","=","0")
            ->select('italy_cities.comune as comune', 'italy_cities.regione as regione', 'italy_cities.provincia as sigla','italy_provincies.provincia as provincia', 'italy_cities.cap as cap');
            $data=$data->get();

       $data->map(function($data) {
           $data->stato = 'Italia';
           return $data;
       });
        return response()->json($data->all());
    }

    public function autocomplete_ext(Request $request) {
        $data = DB::table('italy_cities')
            ->leftJoin("italy_provincies","italy_provincies.provincia","italy_provincies.sigla")
            ->where("italy_cities.comune","LIKE","{$request->input('term')}%")
            ->where("italy_cities.estera","=","1")
            ->select('italy_cities.comune as comune', 'italy_cities.regione as regione', 'italy_cities.provincia as sigla','italy_provincies.provincia as provincia', 'italy_cities.cap as cap');
            $data=$data->get();

       $data->map(function($data) {
           $data->stato = 'Estero';
           return $data;
       });
        return response()->json($data->all());
    }



}