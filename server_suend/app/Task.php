<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $table = 'tasks';

	public function categories() {
		return $this->hasMany('App\Category');
	}

	public function priorities() {
		return $this->hasMany('App\Priority');
	}

	public function user() {
	    return $this->hasOne('App\User');
    }
}
