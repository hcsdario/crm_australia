<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{

    use SoftDeletes; //
    protected $table = 'files';


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
