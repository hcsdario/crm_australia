<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Uploads extends Model
{
    use SoftDeletes;

    protected $table = 'uploads';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'files', 'userid','dir'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
