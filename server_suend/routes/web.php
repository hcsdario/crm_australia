<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'Controller@redirectTo')->name('home')->middleware('auth');
  Route::get('/home', 'Controller@redirectTo')->name('home')->middleware('auth');
  Route::get('/index', 'Controller@redirectTo')->name('home')->middleware('auth');

  Route::get('/admin/profile', 'HomeController@index_admin')->name('profile_admin')->middleware('auth');
  Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard_admin')->middleware('auth');

  Route::get('/files-data/{customer_id}', 'FilesController@data')->name('files_data')->middleware('auth');
  Route::get('/file/delete/{id_file}', 'FileController@logicDelete')->name('delete_file')->middleware('auth');

  Route::get('/news/read/{id}', 'NewsController@readNews')->name('read_news')->middleware('auth');


  Route::resource('uploads', 'uploadsController');
  Route::resource('file', 'FileController');
